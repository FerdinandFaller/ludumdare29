public enum OrganType
{
    Heart,
    Brain,
    Liver,
    Lung,
    Stomach,
    Body
}