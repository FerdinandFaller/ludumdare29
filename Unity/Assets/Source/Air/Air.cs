﻿/// <summary>
/// One Air Instance stands for 1ml of Air. 
/// </summary>
public class Air
{
    public static float OxygenMax { get; private set; }

    private float _oxygen;
    public float Oxygen
    {
        get { return _oxygen; }
        set
        {
            _oxygen = value;
            if (_oxygen > OxygenMax) throw new ExceedingMaximumException();
        }
    }

    public static float CarbonDioxideMax { get; private set; }

    private float _carbonDioxide;
    public float CarbonDioxide
    {
        get { return _carbonDioxide; }
        set
        {
            _carbonDioxide = value;
            if (_carbonDioxide > CarbonDioxideMax) throw new ExceedingMaximumException();
        }
    }


    public Air()
    {
        OxygenMax = 0.025f;
        CarbonDioxideMax = 0.025f;
    }
}