﻿using System.Collections.Generic;
using UnityEngine;

public class Surrounding
{
    private uint _oxygen;
    private uint _carbonDioxide;


    public Surrounding()
    {
        // TODO: Oxygen and CarbonDioxide should change depending on the inputs and outputs
    }

    public Queue<Air> GetAir(uint amount)
    {
        var airs = new Queue<Air>((int)amount);

        for (int i = 0; i < amount; i++)
        {
            // Best Air possible atm
            var air = new Air {CarbonDioxide = 0, Oxygen = Air.OxygenMax};
            airs.Enqueue(air);
        }

        return airs;
    }

    public void ReturnAir(Queue<Air> airs)
    {
        airs.Clear();
        // TODO: This should do something
    }
}