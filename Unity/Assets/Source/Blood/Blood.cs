﻿/// <summary>
/// One Blood Instance stands for 1ml of Blood. 
/// </summary>
public class Blood
{
    public static float NutrientsMax { get; private set; }

    private float _nutrients;
    public float Nutrients
    {
        get { return _nutrients; }
        set
        {
            _nutrients = value;
            if (_nutrients > NutrientsMax) throw new ExceedingMaximumException();
        }
    }

    public static float HarmfulSubstancesMax { get; private set; }

    private float _harmfulSubstances;
    public float HarmfulSubstances
    {
        get { return _harmfulSubstances; }
        set
        {
            _harmfulSubstances = value;
            if (_harmfulSubstances > HarmfulSubstancesMax) throw new ExceedingMaximumException();
        }
    }

    public static float OxygenMax { get; private set; }

    private float _oxygen;
    public float Oxygen
    {
        get { return _oxygen; }
        set
        {
            _oxygen = value;
            if (_oxygen > OxygenMax) throw new ExceedingMaximumException();
        }
    }

    public static float CarbonDioxideMax { get; private set; }

    private float _carbonDioxide;
    public float CarbonDioxide
    {
        get { return _carbonDioxide; }
        set
        {
            _carbonDioxide = value;
            if (_carbonDioxide > CarbonDioxideMax) throw new ExceedingMaximumException();
        }
    }

    public static float WaterMax { get; private set; }

    private float _water;
    public float Water
    {
        get { return _water; }
        set
        {
            _water = value;
            if (_water < 0) _water = 0;
            if (_water > WaterMax) throw new ExceedingMaximumException();
        }
    }

    public static float LeucocytesMax { get; private set; }

    private float _leucocytes;
    /// <summary>
    /// Amount of White Blood Cells.
    /// </summary>
    public float Leucocytes
    {
        get { return _leucocytes; }
        set
        {
            _leucocytes = value;
            if (_leucocytes < 0) _leucocytes = 0;
            if (_leucocytes > LeucocytesMax) throw new ExceedingMaximumException();
        }
    }


    public Blood()
    {
        NutrientsMax = 1;
        HarmfulSubstancesMax = 1;
        OxygenMax = 1;
        CarbonDioxideMax = 0.5f;
        WaterMax = 0.01f;
        LeucocytesMax = 1;

        Nutrients = NutrientsMax * 0.3f;
        Oxygen = OxygenMax * 0.3f;
        Water = WaterMax * 0.3f;
        Leucocytes = LeucocytesMax * 0.3f;
    }
}