﻿public enum BloodVesselWidthType
{
    Narrow,
    Normal,
    Wide,
}