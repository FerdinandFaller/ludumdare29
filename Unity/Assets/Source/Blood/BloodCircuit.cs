﻿using System;
using System.Collections.Generic;
using System.Linq;

public class BloodCircuit
{
    public const int StartBloodAmount = 5000;
    public const uint HeartToBrainDistance = 700;
    public const uint BrainToStomachDistance = 1000;
    public const uint StomachToLiverDistance = 1000;
    public const uint LiverToHeartDistance = 1000;

    public float BloodVesselWidth { get; private set; }
    public Queue<Blood> BloodQueue = new Queue<Blood>(StartBloodAmount);
    
    

    public BloodCircuit()
    {
        SetBloodVesselWidth(BloodVesselWidthType.Normal);

        for (int i = 0; i < StartBloodAmount; i++)
        {
            BloodQueue.Enqueue(new Blood());
        }
    }

    public void SetBloodVesselWidth(BloodVesselWidthType widthType)
    {
        switch (widthType)
        {
            case BloodVesselWidthType.Narrow:
                BloodVesselWidth = 0.75f;
                break;
            case BloodVesselWidthType.Normal:
                BloodVesselWidth = 1f;
                break;
            case BloodVesselWidthType.Wide:
                BloodVesselWidth = 1.25f;
                break;
            default:
                throw new ArgumentOutOfRangeException("widthType");
        }
    }

    public float GetAverageAmountOfNutrients()
    {
        var nutrients = BloodQueue.Sum(blood => blood.Nutrients);

        nutrients /= BloodQueue.Count;

        return nutrients;
    }

    public float GetAverageAmountOfHarmfulSubstances()
    {
        var harmfulSubstances = BloodQueue.Sum(blood => blood.HarmfulSubstances);

        harmfulSubstances /= BloodQueue.Count;

        return harmfulSubstances;
    }

    public float GetAverageAmountOfOxygen()
    {
        var oxygen = BloodQueue.Sum(blood => blood.Oxygen);

        oxygen /= BloodQueue.Count;

        return oxygen;
    }

    public float GetAverageAmountOfCarbonDioxide()
    {
        var carbonDioxide = BloodQueue.Sum(blood => blood.CarbonDioxide);

        carbonDioxide /= BloodQueue.Count;

        return carbonDioxide;
    }

    public float GetAverageAmountOfWater()
    {
        var water = BloodQueue.Sum(blood => blood.Water);

        water /= BloodQueue.Count;

        return water;
    }

    public float GetAverageAmountOfLeucocytes()
    {
        var leucocytes = BloodQueue.Sum(blood => blood.Leucocytes);

        leucocytes /= BloodQueue.Count;

        return leucocytes;
    }
}