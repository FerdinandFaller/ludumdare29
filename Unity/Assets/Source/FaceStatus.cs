public enum FaceStatus
{
    Dead,
    Sick,
    Neutral,
    Happy
}