public enum OrganAlertType
{
    DeHydration,
    Starvation,
    SuffocationReasonOxygen,
    SuffocationReasonCarbonDioxide,
    OverExhaustion,
}