﻿using UnityEngine;

public sealed class BodyModel : OrganModel
{
    public override float DamageWhenSuffocatingDueToCarbonDioxide { get; protected set; }
    public override float HealthRegenerationPerTick { get; protected set; }
    public override float ExhaustionFactor { get; protected set; }
    public override OrganType OrganType { get; protected set; }
    public override string OrganName { get; protected set; }
    public override float DamageWhenOverExhausting { get; protected set; }
    protected override float NutrientConsumptionBase { get; set; }
    public override float DamageWhenStarving { get; protected set; }
    protected override float OxygenConsumptionBase { get; set; }
    public override float DamageWhenSuffocatingDueToOxygen { get; protected set; }
    protected override float WaterConsumptionBase { get; set; }
    public override float DamageWhenDeHydrating { get; protected set; }
    protected override float CarbonDioxideProductionBase { get; set; }
    private float LeucocytesProductionBase { get; set; }
    public float LeucocytesProduction
    {
        get
        {
            return LeucocytesProductionBase * Effort * (1 - (Exhaustion * 0.5f));
        }
    }
    public float LeucocytesExcess { get; set; }
    protected override float EffortFactor { get; set; }

    public BodyModel()
    {
        Effort = 0.5f;
        EffortFactor = 1;
        OrganType = OrganType.Body;
        OrganName = "Body";
        NutrientConsumptionBase = 0;
        OxygenConsumptionBase = 0;
        WaterConsumptionBase = 0;
        CarbonDioxideProductionBase = 0;
        ExhaustionFactor = 0;
        Health = 1;
        DamageWhenDeHydrating = 0;
        DamageWhenOverExhausting = 0;
        DamageWhenStarving = 0;
        DamageWhenSuffocatingDueToCarbonDioxide = 0;
        DamageWhenSuffocatingDueToOxygen = 0;
        HealthRegenerationPerTick = 0;

        LeucocytesProductionBase = 1 / Effort;
    }
}