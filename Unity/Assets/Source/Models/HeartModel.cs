﻿using UnityEngine;

public sealed class HeartModel : OrganModel
{
    public override float DamageWhenSuffocatingDueToCarbonDioxide { get; protected set; }
    public override float HealthRegenerationPerTick { get; protected set; }
    public override float ExhaustionFactor { get; protected set; }
    public override OrganType OrganType { get; protected set; }
    public override string OrganName { get; protected set; }
    public override float DamageWhenOverExhausting { get; protected set; }
    protected override float NutrientConsumptionBase { get; set; }
    public override float DamageWhenStarving { get; protected set; }
    protected override float OxygenConsumptionBase { get; set; }
    public override float DamageWhenSuffocatingDueToOxygen { get; protected set; }
    protected override float WaterConsumptionBase { get; set; }
    public override float DamageWhenDeHydrating { get; protected set; }
    protected override float CarbonDioxideProductionBase { get; set; }
    protected override float EffortFactor { get; set; }

    private uint BloodAmountPerBeatBase { get; set; }
    public uint BloodAmountPerBeat
    {
        get
        {
            return (uint)Mathf.RoundToInt(BloodAmountPerBeatBase * (1 - (Exhaustion * 0.5f)));
        }
    }
    private float HeartBeatFrequencyBase { get; set; }
    public float HeartBeatFrequency
    {
        get
        {
            return HeartBeatFrequencyBase * (Effort * (1 - (Exhaustion * 0.5f)));
        }
    }


    public HeartModel()
    {
        Effort = 0.5f;
        EffortFactor = 1;
        OrganType = OrganType.Heart;
        OrganName = "Heart";
        NutrientConsumptionBase = ((3000f / 5f / 24f / 60f / 60f) * LD29Root.SimulationSpeedFactor) / Effort;
        OxygenConsumptionBase = 5 / Effort;
        WaterConsumptionBase = ((3f / 5f / 24f / 60f / 60f) * LD29Root.SimulationSpeedFactor) / Effort;
        CarbonDioxideProductionBase = 5 / Effort;
        ExhaustionFactor = 0.01f;
        Health = 1;
        DamageWhenDeHydrating = 0.001f;
        DamageWhenOverExhausting = 0.001f;
        DamageWhenStarving = 0.001f;
        DamageWhenSuffocatingDueToCarbonDioxide = 0.001f;
        DamageWhenSuffocatingDueToOxygen = 0.001f;
        HealthRegenerationPerTick = 0.0005f;

        BloodAmountPerBeatBase = (uint) Mathf.Max(Mathf.RoundToInt(70 / Effort), 1);
        HeartBeatFrequencyBase = (70/60f) / Effort; // 70 HeartBeats per Minute
    }
}