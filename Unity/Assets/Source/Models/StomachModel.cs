﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public sealed class StomachModel : OrganModel
{
    [Inject]
    public StomachContentsVolumeChangedSignal StomachContentsVolumeChangedSignal { get; set; }
    [Inject]
    public StomachContentsVolumeAlertChangedSignal StomachContentsVolumeAlertChangedSignal { get; set; }

    public override float DamageWhenSuffocatingDueToCarbonDioxide { get; protected set; }
    public override float HealthRegenerationPerTick { get; protected set; }
    public override float ExhaustionFactor { get; protected set; }
    public override OrganType OrganType { get; protected set; }
    public override string OrganName { get; protected set; }
    public override float DamageWhenOverExhausting { get; protected set; }
    protected override float NutrientConsumptionBase { get; set; }
    public override float DamageWhenStarving { get; protected set; }
    protected override float OxygenConsumptionBase { get; set; }
    public override float DamageWhenSuffocatingDueToOxygen { get; protected set; }
    protected override float WaterConsumptionBase { get; set; }
    public override float DamageWhenDeHydrating { get; protected set; }
    protected override float CarbonDioxideProductionBase { get; set; }
    protected override float EffortFactor { get; set; }

    public float FullThreshold { get; private set; }
    public float PukeThreshold { get; private set; }
    public float HungryThreshold { get; private set; }
    public float EmptyThreshold { get; private set; }

    /// <summary>
    /// [0..2]
    /// </summary>
    public float CurrentContentsVolume
    {
        get { return Contents.Sum(containedFood => containedFood.Volume); }
    }

    public Queue<IFood> Contents { get; private set; }

    public float NutrientsExcess
    {
        get { return _nutrientsExcess; }
        set
        {
            _nutrientsExcess = value;
            if (_nutrientsExcess > NutrientsExcessMax) _nutrientsExcess = NutrientsExcessMax;
            if (_nutrientsExcess < 0) _nutrientsExcess = 0;
        }
    }
    public float NutrientsExcessMax { get; private set; }

    public float HarmfulSubstancesExcess
    {
        get { return _harmfulSubstancesExcess; }
        set
        {
            _harmfulSubstancesExcess = value;
            if (_harmfulSubstancesExcess > HarmfulSubstancesExcessMax) _harmfulSubstancesExcess = HarmfulSubstancesExcessMax;
            if (_harmfulSubstancesExcess < 0) _harmfulSubstancesExcess = 0;
        }
    }
    public float HarmfulSubstancesExcessMax { get; private set; }

    public float WaterExcess
    {
        get { return _waterExcess; }
        set
        {
            _waterExcess = value;
            if (_waterExcess > WaterExcessMax) _waterExcess = WaterExcessMax;
            if (_waterExcess < 0) _harmfulSubstancesExcess = 0;
        }
    }
    public float WaterExcessMax { get; private set; }

    private float _currentContentsVolume;
    private StomachContentsVolumeAlertType _lastStomachContentsVolumeAlert;
    private float _nutrientsExcess;
    private float _harmfulSubstancesExcess;
    private float _waterExcess;


    public StomachModel()
    {
        Effort = 0.5f;
        EffortFactor = 1;
        Contents = new Queue<IFood>();
        OrganType = OrganType.Stomach;
        OrganName = "Stomach";
        NutrientConsumptionBase = ((3000f / 5f / 24f / 60f / 60f) * LD29Root.SimulationSpeedFactor) / Effort;
        OxygenConsumptionBase = 5 / Effort;
        WaterConsumptionBase = ((3f / 5f / 24f / 60f / 60f) * LD29Root.SimulationSpeedFactor) / Effort;
        CarbonDioxideProductionBase = 5 / Effort;
        ExhaustionFactor = 0.01f;
        Health = 1;
        DamageWhenDeHydrating = 0.001f;
        DamageWhenOverExhausting = 0.001f;
        DamageWhenStarving = 0.001f;
        DamageWhenSuffocatingDueToCarbonDioxide = 0.001f;
        DamageWhenSuffocatingDueToOxygen = 0.001f;
        HealthRegenerationPerTick = 0.0005f;

        EmptyThreshold = 0;
        HungryThreshold = 0.1f;
        FullThreshold = 1;
        PukeThreshold = 1.1f;

        NutrientsExcessMax = 100;
        HarmfulSubstancesExcessMax = 100;
        WaterExcessMax = 1;
    }

    public bool AddFood(IFood food, bool forceIn)
    {
        if (!forceIn && CurrentContentsVolume + food.Volume > 1) return false;

        Contents.Enqueue(food);
        if (IsInitialized) StomachContentsVolumeChangedSignal.Dispatch(CurrentContentsVolume);
        DetermineAndDispatchContentsVolumeStatus();

        return true;
    }

    /// <summary>
    /// Only removes Food from the queue if fully digested or forced to.
    /// </summary>
    /// <param name="forceRemove"></param>
    /// <returns></returns>
    public bool RemoveNextFood(bool forceRemove)
    {
        if (Contents.Count == 0) return false;

        var food = Contents.Peek();
        if (forceRemove || (food.HarmfulSubstances <= 0 && food.Nutrients <= 0 && food.Water <= 0))
        {
            Contents.Dequeue();
            if (IsInitialized) StomachContentsVolumeChangedSignal.Dispatch(CurrentContentsVolume);
            DetermineAndDispatchContentsVolumeStatus();
            return true;
        }

        return false;
    }

    private void DetermineAndDispatchContentsVolumeStatus()
    {
        StomachContentsVolumeAlertType temp;

        if (CurrentContentsVolume >= PukeThreshold)
        {
            temp = StomachContentsVolumeAlertType.Puke;
        }
        else if (CurrentContentsVolume >= FullThreshold)
        {
            temp = StomachContentsVolumeAlertType.Full;
        }
        else if (CurrentContentsVolume < FullThreshold && CurrentContentsVolume > HungryThreshold)
        {
            // No Alert
            return;
        }
        else if (CurrentContentsVolume <= HungryThreshold && CurrentContentsVolume > EmptyThreshold)
        {
            temp = StomachContentsVolumeAlertType.Hungry;
        }
        else
        {
            temp = StomachContentsVolumeAlertType.Empty;
        }

        if (_lastStomachContentsVolumeAlert != temp)
        {
            _lastStomachContentsVolumeAlert = temp;
            StomachContentsVolumeAlertChangedSignal.Dispatch(_lastStomachContentsVolumeAlert);
        }
    }
}