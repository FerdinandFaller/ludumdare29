﻿using System.Collections.Generic;
using UnityEngine;

public sealed class LungModel : OrganModel
{
    public override float DamageWhenSuffocatingDueToCarbonDioxide { get; protected set; }
    public override float HealthRegenerationPerTick { get; protected set; }
    public override float ExhaustionFactor { get; protected set; }
    public override OrganType OrganType { get; protected set; }
    public override string OrganName { get; protected set; }
    public override float DamageWhenOverExhausting { get; protected set; }
    protected override float NutrientConsumptionBase { get; set; }
    public override float DamageWhenStarving { get; protected set; }
    protected override float OxygenConsumptionBase { get; set; }
    public override float DamageWhenSuffocatingDueToOxygen { get; protected set; }
    protected override float WaterConsumptionBase { get; set; }
    public override float DamageWhenDeHydrating { get; protected set; }
    protected override float CarbonDioxideProductionBase { get; set; }
    protected override float EffortFactor { get; set; }

    private float BreathingFrequencyBase { get; set; }
    public float BreathingFrequency
    {
        get
        {
            return BreathingFrequencyBase * (Effort * (1 - (Exhaustion * 0.5f)));
        }
    }

    public uint LungVolume { get; set; }
    public Queue<Air> AirQueue;
    /// <summary>
    /// [0...1] The Lung exchanges the oxygen in the air it currently holds by this factor into the blood it receives each HeartBeat.
    /// </summary>
    public float OxygenExchangeFactor { get; set; }
    /// <summary>
    /// [0...1] The Lung exchanges the carbondioxide in the air it currently holds by this factor into the blood it receives each HeartBeat.
    /// </summary>
    public float CarbonDioxideExchangeFactor { get; set; }


    public LungModel()
    {
        Effort = 0.5f;
        EffortFactor = 1;
        OrganType = OrganType.Lung;
        OrganName = "Lung";
        NutrientConsumptionBase = ((3000f / 5f / 24f / 60f / 60f) * LD29Root.SimulationSpeedFactor) / Effort;
        OxygenConsumptionBase = 5 / Effort;
        WaterConsumptionBase = ((3f / 5f / 24f / 60f / 60f) * LD29Root.SimulationSpeedFactor) / Effort;
        CarbonDioxideProductionBase = 5 / Effort;
        ExhaustionFactor = 0.01f;
        Health = 1;
        DamageWhenDeHydrating = 0.001f;
        DamageWhenOverExhausting = 0.001f;
        DamageWhenStarving = 0.001f;
        DamageWhenSuffocatingDueToCarbonDioxide = 0.001f;
        DamageWhenSuffocatingDueToOxygen = 0.001f;
        HealthRegenerationPerTick = 0.0005f;

        LungVolume = 5000;
        AirQueue = new Queue<Air>((int) LungVolume);
        BreathingFrequencyBase = (15 / 60f) / Effort; // 15 Breathings per Minute
        OxygenExchangeFactor = 1;
        CarbonDioxideExchangeFactor = 1;
    }
}