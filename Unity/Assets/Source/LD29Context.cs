﻿using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using UnityEngine;

public class LD29Context : MVCSContext
{
    public LD29Context(MonoBehaviour view, bool autoStartup) : base(view, autoStartup)
    {
    }

    protected override void addCoreComponents()
    {
        base.addCoreComponents();
        injectionBinder.Unbind<ICommandBinder>();
        injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
    }

    override public void Launch()
    {
        base.Launch();
        //Make sure you've mapped this to a StartCommand!
        var startSignal = injectionBinder.GetInstance<StartSignal>();
        startSignal.Dispatch();
    }

    protected override void mapBindings()
    {
        // CommandBindings
        commandBinder.Bind<StartSignal>().To<StartCommand>();
        commandBinder.Bind<UpdateSignal>().To<UpdateCommand>();

        commandBinder.Bind<ShowHideGuiViewSignal>().To<ShowHideGuiViewCommand>();
        commandBinder.Bind<HideAllOrganGUIInfoViewsSignal>().To<HideAllOrganGUIInfoViewsCommand>();
        commandBinder.Bind<MainMenuStartButtonClickedSignal>().To<MainMenuStartButtonClickedCommand>();

        commandBinder.Bind<RequestSettingOfGenericOrganInfoSignal>().To<RequestSettingOfGenericOrganInfoCommand>();
        commandBinder.Bind<RequestSettingOfHeartInfoSignal>().To<RequestSettingOfHeartInfoCommand>();
        commandBinder.Bind<RequestSettingOfBrainInfoSignal>().To<RequestSettingOfBrainInfoCommand>();
        commandBinder.Bind<RequestSettingOfLungInfoSignal>().To<RequestSettingOfLungInfoCommand>();
        commandBinder.Bind<RequestSettingOfLiverInfoSignal>().To<RequestSettingOfLiverInfoCommand>();
        commandBinder.Bind<RequestSettingOfStomachInfoSignal>().To<RequestSettingOfStomachInfoCommand>();
        commandBinder.Bind<RequestAverageOrganHealthSignal>().To<RequestAverageOrganHealthCommand>();
        commandBinder.Bind<RequestFaceStatusSignal>().To<RequestFaceStatusCommand>();

        commandBinder.Bind<OrganEffortChangeSignal>().To<OrganEffortChangeCommand>();
        commandBinder.Bind<OrganHealthChangeSignal>().To<OrganHealthChangeCommand>();
        commandBinder.Bind<DetermineOrganStatusSignal>().To<DetermineOrganStatusCommand>();
        commandBinder.Bind<GenericOrganHeartBeatSignal>().To<GenericOrganHeartBeatCommand>();
        commandBinder.Bind<HeartBeatSignal>().To<HeartBeatCommand>();
        commandBinder.Bind<SimulationTickSignal>().To<SimulationTickCommand>();
        commandBinder.Bind<UpdateOrganExhaustionSignal>().To<UpdateOrganExhaustionCommand>();
        commandBinder.Bind<DigestSignal>().To<DigestCommand>();
        commandBinder.Bind<IncreaseOrganDeficitsAndExcessesSignal>().To<IncreaseOrganDeficitsAndExcessesCommand>();
        commandBinder.Bind<OrganAlertSignal>().To<OrganAlertCommand>();
        commandBinder.Bind<CheckForOrganAlertsSignal>().To<CheckForOrganAlertsCommand>();
        commandBinder.Bind<OrganHealthDecreaseSignal>().To<OrganHealthDecreaseCommand>();
        commandBinder.Bind<EatSignal>().To<EatCommand>();
        commandBinder.Bind<OrganHealthRegenerationSignal>().To<OrganHealthRegenerationCommand>();
        commandBinder.Bind<AirToBloodOxygenExchangeSignal>().To<AirToBloodOxygenExchangeCommand>();
        commandBinder.Bind<BloodToAirCarbonDioxideExchangeSignal>().To<BloodToAirCarbonDioxideExchangeCommand>();
        commandBinder.Bind<BreathInSignal>().To<BreathInCommand>();
        commandBinder.Bind<BreathOutSignal>().To<BreathOutCommand>();
        
        mediationBinder.Bind<MainMenuView>().To<MainMenuMediator>();
        mediationBinder.Bind<GameUIView>().To<GameUIMediator>();
        mediationBinder.Bind<GenericOrganInfoView>().To<GenericOrganInfoMediator>();


        // Organ Views and Models
        mediationBinder.Bind<BodyView>().To<BodyMediator>();
        injectionBinder.Bind<BodyModel>().ToSingleton();

        mediationBinder.Bind<FaceView>().To<FaceMediator>();

        mediationBinder.Bind<BrainView>().To<BrainMediator>();
        injectionBinder.Bind<BrainModel>().ToSingleton();

        mediationBinder.Bind<LungView>().To<LungMediator>();
        injectionBinder.Bind<LungModel>().ToSingleton();

        mediationBinder.Bind<LiverView>().To<LiverMediator>();
        injectionBinder.Bind<LiverModel>().ToSingleton();

        mediationBinder.Bind<StomachView>().To<StomachMediator>();
        injectionBinder.Bind<StomachModel>().ToSingleton();

        mediationBinder.Bind<HeartView>().To<HeartMediator>();
        injectionBinder.Bind<HeartModel>().ToSingleton();

        injectionBinder.Bind<BloodCircuit>().ToSingleton();
        injectionBinder.Bind<Surrounding>().ToSingleton();

        // Signals without Commands
        injectionBinder.Bind<OrganExhaustionChangedSignal>().ToSingleton();
        injectionBinder.Bind<OrganEffortChangedSignal>().ToSingleton();
        injectionBinder.Bind<SetGenericOrganInfoSignal>().ToSingleton();
        injectionBinder.Bind<SetHeartInfoSignal>().ToSingleton();
        injectionBinder.Bind<SetBrainInfoSignal>().ToSingleton();
        injectionBinder.Bind<SetLungInfoSignal>().ToSingleton();
        injectionBinder.Bind<SetLiverInfoSignal>().ToSingleton();
        injectionBinder.Bind<SetStomachInfoSignal>().ToSingleton();
        injectionBinder.Bind<StomachContentsVolumeChangedSignal>().ToSingleton();
        injectionBinder.Bind<StomachContentsVolumeAlertChangedSignal>().ToSingleton();
        injectionBinder.Bind<OrganStatusChangedSignal>().ToSingleton();
        injectionBinder.Bind<OrganHealthChangedSignal>().ToSingleton();
        injectionBinder.Bind<AverageOrganHealthSignal>().ToSingleton();
        injectionBinder.Bind<FaceStatusSignal>().ToSingleton();
    }
}