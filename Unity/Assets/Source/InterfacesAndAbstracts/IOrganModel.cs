﻿public interface IOrganModel
{
    OrganEffortChangedSignal OrganEffortChangedSignal { get; set; }
    OrganExhaustionChangedSignal OrganExhaustionChangedSignal { get; set; }
    OrganStatusChangedSignal OrganStatusChangedSignal { get; set; }
    OrganHealthChangedSignal OrganHealthChangedSignal { get; set; }

    OrganType OrganType { get; }
    string OrganName { get; }
    OrganStatus OrganStatus { get; set; }

    float NutrientConsumption { get; }
    float NutrientDeficit { get; set; }
    float NutrientDeficitDamageThreshold { get; }
    float NutrientDeficitWarningThreshold { get; }
    float DamageWhenStarving { get; }
    OrganAlertSeverness StarvationAlertSeverness { get; }

    float OxygenConsumption { get; }
    float OxygenDeficit { get; set; }
    float OxygenDeficitDamageThreshold { get; }
    float OxygenDeficitWarningThreshold { get; }
    float DamageWhenSuffocatingDueToOxygen { get; }
    OrganAlertSeverness SuffocationDueToOxygenAlertSeverness { get; }

    float WaterConsumption { get; }
    float WaterDeficit { get; set; }
    float WaterDeficitDamageThreshold { get; }
    float WaterDeficitWarningThreshold { get; }
    float DamageWhenDeHydrating { get; }
    OrganAlertSeverness DeHydrationAlertSeverness { get; }

    float CarbonDioxideProduction { get; }
    float CarbonDioxideExcess { get; set; }
    float CarbonDioxideExcessDamageThreshold { get; }
    float CarbonDioxideExcessWarningThreshold { get; }
    float DamageWhenSuffocatingDueToCarbonDioxide { get; }
    OrganAlertSeverness SuffocationDueToCarbonDioxideAlertSeverness { get; }

    

    /// <summary>
    /// How fast is an increased Effort(>0.5) applied to Exhaustion?
    /// </summary>
    float ExhaustionFactor { get; }
    /// <summary>
    /// [0..1] 0.5 = Default
    /// </summary>
    float Effort { get; set; }
    float Exhaustion { get; set; }
    float ExhaustionDamageThreshold { get; }
    float ExhaustionWarningThreshold { get; }
    float DamageWhenOverExhausting { get; }
    OrganAlertSeverness OverExhaustionAlertSeverness { get; }
    float Health { get; set; }
    float HealthRegenerationPerTick { get; }
}