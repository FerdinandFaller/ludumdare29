﻿using UnityEngine;

public abstract class OrganModel : IOrganModel
{
    #region Signals

    [Inject]
    public OrganEffortChangedSignal OrganEffortChangedSignal { get; set; }
    [Inject]
    public OrganExhaustionChangedSignal OrganExhaustionChangedSignal { get; set; }
    [Inject]
    public OrganStatusChangedSignal OrganStatusChangedSignal { get; set; }
    [Inject]
    public OrganHealthChangedSignal OrganHealthChangedSignal { get; set; }
    [Inject]
    public OrganAlertSignal OrganAlertSignal { get; set; }

    #endregion // Signals

    public abstract OrganType OrganType { get; protected set; }

    public abstract string OrganName { get; protected set; }

    public OrganStatus OrganStatus
    {
        get { return _organStatus; }
        set
        {
            var before = _organStatus;
            _organStatus = value;
            if (IsInitialized && before != _organStatus) OrganStatusChangedSignal.Dispatch(OrganType, _organStatus);
        }
    }

    #region Health

    public abstract float HealthRegenerationPerTick { get; protected set; }

    public float Health
    {
        get { return _health; }
        set
        {
            var before = _health;
            _health = value;

            if (_health < 0) _health = 0;
            if (_health > 1) _health = 1;

            if (Mathf.Abs(before - _health) > 0.000001f)
            {
                if (IsInitialized) OrganHealthChangedSignal.Dispatch(OrganType, _health);
            }
        }
    }

    #endregion // Health

    #region Effort

    public float Effort
    {
        get { return _effort * EffortFactor; }
        set
        {
            var before = _effort;
            _effort = value;

            if (_effort < 0) _effort = 0;
            if (_effort > 1) _effort = 1;

            if (Mathf.Abs(before - _effort) > 0.0001f)
            {
                if (IsInitialized) OrganEffortChangedSignal.Dispatch(OrganType, _effort);
            }
        }
    }

    /// <summary>
    /// How 'efficient' is 'Effort' applied?
    /// </summary>
    protected abstract float EffortFactor { get; set; }

    #endregion // Effort

    #region Exhaustion

    public float Exhaustion
    {
        get { return _exhaustion; }
        set
        {
            var before = _exhaustion;
            _exhaustion = value;

            if (_exhaustion < 0) _exhaustion = 0;
            if (_exhaustion > 1) _exhaustion = 1;

            if (Mathf.Abs(before - _exhaustion) > 0.000001f)
            {
                if (IsInitialized) OrganExhaustionChangedSignal.Dispatch(OrganType, _exhaustion);
            }
        }
    }
    public abstract float ExhaustionFactor { get; protected set; }
    public float ExhaustionDamageThreshold { get; private set; }
    public float ExhaustionWarningThreshold { get; private set; }
    public abstract float DamageWhenOverExhausting { get; protected set; }
    public OrganAlertSeverness OverExhaustionAlertSeverness
    {
        get
        {
            if (Exhaustion > ExhaustionDamageThreshold)
            {
                return OrganAlertSeverness.Damage;
            }
            if (Exhaustion > ExhaustionWarningThreshold)
            {
                return OrganAlertSeverness.Warning;
            }
            return OrganAlertSeverness.Ok;
        }
    }

    #endregion // Exhaustion

    #region Nutrients

    protected abstract float NutrientConsumptionBase { get; set; }
    public float NutrientConsumption
    {
        get
        {
            return NutrientConsumptionBase * Effort *  (1 - (Exhaustion * 0.5f));
        }
    }

    public float NutrientDeficit
    {
        get { return _nutrientDeficit; }
        set
        {
            _nutrientDeficit = value;
            if (_nutrientDeficit > NutrientDeficitMax) _nutrientDeficit = NutrientDeficitMax;
            if (_nutrientDeficit < 0) _nutrientDeficit = 0;
        }
    }

    public float NutrientDeficitMax { get; private set; }
    public float NutrientDeficitDamageThreshold { get; private set; }
    public float NutrientDeficitWarningThreshold { get; private set; }
    public abstract float DamageWhenStarving { get; protected set; }
    public OrganAlertSeverness StarvationAlertSeverness
    {
        get
        {
            if (NutrientDeficit > NutrientDeficitDamageThreshold)
            {
                return OrganAlertSeverness.Damage;
            }
            if (NutrientDeficit > NutrientDeficitWarningThreshold)
            {
                return OrganAlertSeverness.Warning;
            }
            return OrganAlertSeverness.Ok;
        }
    }

    #endregion // Nutrients

    #region Oxygen

    protected abstract float OxygenConsumptionBase { get; set; }
    public float OxygenConsumption
    {
        get
        {
            return OxygenConsumptionBase * Effort * (1 - (Exhaustion * 0.5f));
        }
    }

    public float OxygenDeficit
    {
        get { return _oxygenDeficit; }
        set
        {
            _oxygenDeficit = value;
            if (_oxygenDeficit > OxygenDeficitMax) _oxygenDeficit = OxygenDeficitMax;
            if (_oxygenDeficit < 0) _oxygenDeficit = 0;
        }
    }

    public float OxygenDeficitMax { get; private set; }
    public float OxygenDeficitDamageThreshold { get; private set; }
    public float OxygenDeficitWarningThreshold { get; private set; }
    public abstract float DamageWhenSuffocatingDueToOxygen { get; protected set; }
    public OrganAlertSeverness SuffocationDueToOxygenAlertSeverness
    {
        get
        {
            if (OxygenDeficit > OxygenDeficitDamageThreshold)
            {
                return OrganAlertSeverness.Damage;
            }
            if (OxygenDeficit > OxygenDeficitWarningThreshold)
            {
                return OrganAlertSeverness.Warning;
            }
            return OrganAlertSeverness.Ok;
        }
    }

    #endregion // Oxygen

    #region Water

    protected abstract float WaterConsumptionBase { get; set; }
    public float WaterConsumption
    {
        get
        {
            return WaterConsumptionBase * Effort * (1 - (Exhaustion * 0.5f));
        }
    }

    public float WaterDeficit
    {
        get { return _waterDeficit; }
        set
        {
            _waterDeficit = value;
            if (_waterDeficit > WaterDeficitMax) _waterDeficit = WaterDeficitMax;
            if (_waterDeficit < 0) _waterDeficit = 0;
        }
    }

    public float WaterDeficitMax { get; private set; }
    public float WaterDeficitDamageThreshold { get; private set; }
    public float WaterDeficitWarningThreshold { get; private set; }
    public abstract float DamageWhenDeHydrating { get; protected set; }

    public OrganAlertSeverness DeHydrationAlertSeverness
    {
        get
        {
            if (WaterDeficit > WaterDeficitDamageThreshold)
            {
                return OrganAlertSeverness.Damage;
            }
            if (WaterDeficit > WaterDeficitWarningThreshold)
            {
                return OrganAlertSeverness.Warning;
            }
            return OrganAlertSeverness.Ok;
        }
    }

    #endregion // Water

    #region CarbonDioxide

    protected abstract float CarbonDioxideProductionBase { get; set; }
    public float CarbonDioxideProduction
    {
        get
        {
            return CarbonDioxideProductionBase * Effort * (1 - (Exhaustion * 0.5f));
        }
    }

    public float CarbonDioxideExcess
    {
        get { return _carbonDioxideExcess; }
        set
        {
            _carbonDioxideExcess = value;
            if (_carbonDioxideExcess > CarbonDioxideExcessMax) _carbonDioxideExcess = CarbonDioxideExcessMax;
            if (_carbonDioxideExcess < 0) _carbonDioxideExcess = 0;
        }
    }

    public float CarbonDioxideExcessMax { get; private set; }
    public float CarbonDioxideExcessDamageThreshold { get; private set; }
    public float CarbonDioxideExcessWarningThreshold { get; private set; }
    public abstract float DamageWhenSuffocatingDueToCarbonDioxide { get; protected set; }

    public OrganAlertSeverness SuffocationDueToCarbonDioxideAlertSeverness
    {
        get
        {
            if (CarbonDioxideExcess > CarbonDioxideExcessDamageThreshold)
            {
                return OrganAlertSeverness.Damage;
            }
            if (CarbonDioxideExcess > CarbonDioxideExcessWarningThreshold)
            {
                return OrganAlertSeverness.Warning;
            }
            return OrganAlertSeverness.Ok;
        }
    }

    #endregion // CarbonDioxide

    protected bool IsInitialized;

    private float _health;
    private float _effort;
    private float _exhaustion;
    private OrganStatus _organStatus;
    private float _nutrientDeficit;
    private float _oxygenDeficit;
    private float _waterDeficit;
    private float _carbonDioxideExcess;


    [PostConstruct]
    public void PostConstruct()
    {
        IsInitialized = true;

        ExhaustionWarningThreshold = 0.7f;
        ExhaustionDamageThreshold = 0.9f;

        NutrientDeficitWarningThreshold = NutrientConsumption * 5f;
        NutrientDeficitDamageThreshold = NutrientConsumption * 20f;
        NutrientDeficitMax = NutrientConsumption * 21f;

        OxygenDeficitWarningThreshold = OxygenConsumption * 5f;
        OxygenDeficitDamageThreshold = OxygenConsumption * 20f;
        OxygenDeficitMax = OxygenConsumption * 21f;

        WaterDeficitWarningThreshold = WaterConsumption * 5f;
        WaterDeficitDamageThreshold = WaterConsumption * 20f;
        WaterDeficitMax = WaterConsumption * 21f;

        CarbonDioxideExcessWarningThreshold = CarbonDioxideProduction * 5f;
        CarbonDioxideExcessDamageThreshold = CarbonDioxideProduction * 20f;
        CarbonDioxideExcessMax = CarbonDioxideProduction * 21f;
    }
}