﻿using UnityEngine;

public abstract class Food : IFood
{
    public abstract float Nutrients { get; protected set; }
    public abstract float MaxNutrientsDigestionPerTick { get; protected set; }
    public abstract float HarmfulSubstances { get; protected set; }
    public abstract float MaxHarmfulSubstancesDigestionPerTick { get; protected set; }
    public abstract float Water { get; protected set; }
    public abstract float MaxWaterDigestionPerTick { get; protected set; }
    public abstract float Volume { get; protected set; }


    public void DigestTick(float effort, out float digestedNutrients, out float digestedHarmfulSubstances, out float digestedWater)
    {
        var maxNutrientsDigestionPerTickEffortApplied = Mathf.Max(MaxNutrientsDigestionPerTick * effort, 1);
        var maxHarmfulSubstancesDigestionPerTickEffortApplied = Mathf.Max(MaxHarmfulSubstancesDigestionPerTick * effort, 1);
        var maxWaterDigestionPerTickEffortApplied = MaxWaterDigestionPerTick * effort;

        // Nutrients
        if (Nutrients >= maxNutrientsDigestionPerTickEffortApplied)
        {
            digestedNutrients = maxNutrientsDigestionPerTickEffortApplied;
            Nutrients -= maxNutrientsDigestionPerTickEffortApplied;
        }
        else
        {
            digestedNutrients = Nutrients;
            Nutrients = 0;
        }

        // Harmful Substances
        if (HarmfulSubstances >= maxHarmfulSubstancesDigestionPerTickEffortApplied)
        {
            digestedHarmfulSubstances = maxHarmfulSubstancesDigestionPerTickEffortApplied;
            HarmfulSubstances -= maxHarmfulSubstancesDigestionPerTickEffortApplied;
        }
        else
        {
            digestedHarmfulSubstances = HarmfulSubstances;
            HarmfulSubstances = 0;
        }

        // Water
        if (Water >= maxWaterDigestionPerTickEffortApplied)
        {
            digestedWater = maxWaterDigestionPerTickEffortApplied;
            Water -= maxWaterDigestionPerTickEffortApplied;
        }
        else
        {
            digestedWater = Water;
            Water = 0;
        }
    }
}