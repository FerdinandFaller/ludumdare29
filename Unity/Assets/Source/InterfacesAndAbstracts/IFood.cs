﻿public interface IFood
{
    float Nutrients { get; }
    float MaxNutrientsDigestionPerTick { get; }
    float HarmfulSubstances { get; }
    float MaxHarmfulSubstancesDigestionPerTick { get; }
    /// <summary>
    /// In Liter
    /// </summary>
    float Water { get; }
    float MaxWaterDigestionPerTick { get; }
    
    /// <summary>
    /// [0..1] 1 = One Unit is enough to be full for a standard person with empty stomach. 
    /// </summary>
    float Volume { get; }

    void DigestTick(float effort, out float digestedNutrients, out float digestedHarmfulSubstances, out float digestedWater);
}