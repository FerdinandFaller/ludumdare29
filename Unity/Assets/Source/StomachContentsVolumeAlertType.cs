public enum StomachContentsVolumeAlertType
{
    Empty,
    Hungry,
    Full,
    Puke,
}