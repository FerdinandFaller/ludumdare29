using System;
using strange.extensions.command.impl;

public static class CommandExtension
{
    public static IOrganModel GetOrganModelFromOrganType(this Command command, OrganType organType)
    {
        IOrganModel organModel;

        switch (organType)
        {
            case OrganType.Body:
                organModel = command.injectionBinder.GetInstance<BodyModel>();
                break;
            case OrganType.Heart:
                organModel = command.injectionBinder.GetInstance<HeartModel>();
                break;
            case OrganType.Brain:
                organModel = command.injectionBinder.GetInstance<BrainModel>();
                break;
            case OrganType.Liver:
                organModel = command.injectionBinder.GetInstance<LiverModel>();
                break;
            case OrganType.Lung:
                organModel = command.injectionBinder.GetInstance<LungModel>();
                break;
            case OrganType.Stomach:
                organModel = command.injectionBinder.GetInstance<StomachModel>();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        return organModel;
    }
}