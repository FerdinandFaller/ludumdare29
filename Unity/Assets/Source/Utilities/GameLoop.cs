using UnityEngine;

public class GameLoop : MonoBehaviour
{
    public UpdateSignal UpdateSignal { get; set; }
	
	private bool _isRunning;


	public void StartLoop()
	{
		_isRunning = true;
	}
		
	public void StopLoop()
	{
		_isRunning = false;
	}
		
	void Update()
	{
        if (_isRunning && UpdateSignal != null)
		{
		    UpdateSignal.Dispatch(Time.smoothDeltaTime);
		}

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
	}
}