﻿using UnityEngine;

public class BubbleUpOnMouseDown : MonoBehaviour 
{
    void OnMouseDown()
    {
        transform.parent.SendMessage("OnMouseDown", SendMessageOptions.DontRequireReceiver);
    }
}
