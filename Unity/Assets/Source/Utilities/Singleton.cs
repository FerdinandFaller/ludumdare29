﻿using UnityEngine;

/// <summary>
/// Used as a base class for Singletons. 
/// If the subclass contains a method called "InitForSingleton",
/// it will get called directly after the _instance was created.
/// </summary>
/// <typeparam name="T"></typeparam>
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;

    /// <summary>
    /// Returns the instance of this singleton.
    /// </summary>
    /// <value>The instance.</value>
    public static T Instance
    {
        get
        {
            CheckAndCreate();

            return _instance;
        }
    }

    protected virtual void Start()
    {
        CheckAndCreate();
    }

    private static void Create()
    {
        var go = new GameObject();
        _instance = go.AddComponent<T>();
        go.name = typeof(T).Name;
        _instance.SendMessage("InitForSingleton", SendMessageOptions.DontRequireReceiver);
    }

    private static void CheckAndCreate()
    {
        if (_instance == null)
        {
            _instance = FindObjectOfType<T>();

            // If one doesn't exist, make it!
            if (_instance == null) Create();
        }
    }
}