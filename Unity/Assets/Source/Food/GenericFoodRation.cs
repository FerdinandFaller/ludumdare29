public sealed class GenericFoodRation : Food
{
    public override float Nutrients { get; protected set; }
    public override float MaxNutrientsDigestionPerTick { get; protected set; }
    public override float HarmfulSubstances { get; protected set; }
    public override float MaxHarmfulSubstancesDigestionPerTick { get; protected set; }
    public override float Water { get; protected set; }
    public override float MaxWaterDigestionPerTick { get; protected set; }
    public override float Volume { get; protected set; }


    public GenericFoodRation()
    {
        Nutrients = 300;
        MaxNutrientsDigestionPerTick = 0.05f * LD29Root.SimulationSpeedFactor;
        HarmfulSubstances = 1;
        MaxHarmfulSubstancesDigestionPerTick = 0.01f * LD29Root.SimulationSpeedFactor;
        Water = 0.01f;
        MaxWaterDigestionPerTick = 0.01f * LD29Root.SimulationSpeedFactor;
        Volume = 0.1f;
    }
}