﻿public sealed class MineralWater : Food
{
    public override float Nutrients { get; protected set; }
    public override float MaxNutrientsDigestionPerTick { get; protected set; }
    public override float HarmfulSubstances { get; protected set; }
    public override float MaxHarmfulSubstancesDigestionPerTick { get; protected set; }
    public override float Water { get; protected set; }
    public override float MaxWaterDigestionPerTick { get; protected set; }
    public override float Volume { get; protected set; }


    public MineralWater()
    {
        Nutrients = 10;
        MaxNutrientsDigestionPerTick = 0.05f * LD29Root.SimulationSpeedFactor;
        HarmfulSubstances = 1;
        MaxHarmfulSubstancesDigestionPerTick = 0.01f * LD29Root.SimulationSpeedFactor;
        Water = 1;
        MaxWaterDigestionPerTick = 0.000035f * LD29Root.SimulationSpeedFactor;
        Volume = 0.2f;
    }
}