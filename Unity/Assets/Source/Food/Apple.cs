﻿using System;

public sealed class Apple : Food
{
    public override float Nutrients { get; protected set; }
    public override float MaxNutrientsDigestionPerTick { get; protected set; }
    public override float HarmfulSubstances { get; protected set; }
    public override float MaxHarmfulSubstancesDigestionPerTick { get; protected set; }
    public override float Water { get; protected set; }
    public override float MaxWaterDigestionPerTick { get; protected set; }
    
    public override float Volume { get; protected set; }


    public Apple()
    {
        throw new NotImplementedException();
        Nutrients = 50;
        MaxNutrientsDigestionPerTick = 50;
        HarmfulSubstances = 1;
        MaxHarmfulSubstancesDigestionPerTick = 1;
        Water = 0.1f;
        MaxWaterDigestionPerTick = 0.1f;
        Volume = 0.1f;
    }
}