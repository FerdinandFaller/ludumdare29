using System.Collections.Generic;
using strange.extensions.mediation.impl;

public class StomachMediator : Mediator
{
    [Inject]
    public StomachView View { get; set; }

    [Inject]
    public HideAllOrganGUIInfoViewsSignal HideAllOrganGuiInfoViewsSignal { get; set; }
    [Inject]
    public ShowHideGuiViewSignal ShowHideGuiViewSignal { get; set; }
    [Inject]
    public RequestSettingOfGenericOrganInfoSignal RequestSettingOfGenericOrganInfoSignal { get; set; }
    [Inject]
    public RequestSettingOfStomachInfoSignal RequestSettingOfStomachInfoSignal { get; set; }
    [Inject]
    public SetStomachInfoSignal SetStomachInfoSignal { get; set; }
    [Inject]
    public HeartBeatSignal HeartBeatSignal { get; set; }
    [Inject]
    public OrganStatusChangedSignal OrganStatusChangedSignal { get; set; }
    [Inject]
    public StomachContentsVolumeChangedSignal StomachContentsVolumeChangedSignal { get; set; }


    public override void OnRegister()
    {
        View.MouseDownEvent -= OnViewMouseDown;
        View.MouseDownEvent += OnViewMouseDown;

        HeartBeatSignal.AddListener(OnHeartBeat);
        SetStomachInfoSignal.AddListener(OnSetStomachInfo);
        OrganStatusChangedSignal.AddListener(OnOrganStatusChanged);
        StomachContentsVolumeChangedSignal.AddListener(OnStomachContentsVolumeChanged);
    }

    public override void OnRemove()
    {
        View.MouseDownEvent -= OnViewMouseDown;

        HeartBeatSignal.RemoveListener(OnHeartBeat);
        SetStomachInfoSignal.RemoveListener(OnSetStomachInfo);
        OrganStatusChangedSignal.RemoveListener(OnOrganStatusChanged);
        StomachContentsVolumeChangedSignal.RemoveListener(OnStomachContentsVolumeChanged);
    }

    private void OnStomachContentsVolumeChanged(float volume)
    {
        View.SetContentsVolumeSprite(volume);
    }

    private void OnOrganStatusChanged(OrganType organType, OrganStatus status)
    {
        if (organType == OrganType.Stomach) View.SetStatusSprite(status);
    }

    private void OnHeartBeat(OrganType organType, Queue<Blood> bloods)
    {
        if (organType != OrganType.Stomach) return;
    }

    private void OnViewMouseDown()
    {
        HideAllOrganGuiInfoViewsSignal.Dispatch();
        RequestSettingOfGenericOrganInfoSignal.Dispatch(OrganType.Stomach);
        RequestSettingOfStomachInfoSignal.Dispatch();
        ShowHideGuiViewSignal.Dispatch(typeof(GenericOrganInfoView), true);
        ShowHideGuiViewSignal.Dispatch(typeof(StomachView), true);
    }

    private void OnSetStomachInfo()
    {

    }
}