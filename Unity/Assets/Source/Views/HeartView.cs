﻿using System;
using strange.extensions.mediation.impl;
using UnityEngine;

public class HeartView : View
{
    public UILabel BloodPerTickLabel;
    public UILabel TickFrequencyLabel;

    public event Action MouseDownEvent;

    public Animation PumpAnimation;
    public SpriteRenderer Renderer;
    public Sprite GoodSprite;
    public Sprite BadSprite;
    public Sprite UglySprite;

    private uint _pumpAnimationStartFPS;


    void OnMouseDown()
    {
        if (MouseDownEvent != null) MouseDownEvent();
    }

    public void SetBloodPerTickLabel(uint newValue)
    {
        BloodPerTickLabel.text = "Blood Per Heartbeat: " + newValue;
    }

    public void SetHeartBeatFrequencyLabel(float newValue)
    {
        TickFrequencyLabel.text = (newValue * 60f) + " Heartbeats per Minute";
    }

    public void SetPumpAnimationFramerate(int framerate)
    {
        PumpAnimation.clip.frameRate = framerate;
    }

    public void PlayPumpAnimation()
    {
        PumpAnimation.Play();
    }

    public void SetStatusSprite(OrganStatus status)
    {
        switch (status)
        {
            case OrganStatus.Good:
                Renderer.sprite = GoodSprite;
                break;
            case OrganStatus.Bad:
                Renderer.sprite = BadSprite;
                break;
            case OrganStatus.Ugly:
                Renderer.sprite = UglySprite;
                break;
            default:
                throw new ArgumentOutOfRangeException("status");
        }
    }
}