﻿using System;
using strange.extensions.mediation.impl;
using UnityEngine;

public class GenericOrganInfoView : View
{
    public UILabel NameLabel;
    public UILabel HealthLabel;
    public UISlider EffortSlider;
    public UILabel EffortValueLabel;

    public OrganAlertIcon DeHydration;
    public OrganAlertIcon Starvation;
    public OrganAlertIcon SuffocationC;
    public OrganAlertIcon SuffocationO;
    public OrganAlertIcon OverExhaustion;

    public event Action<float> EffortSliderChangedEvent;

    protected override void Start()
    {
        EffortSlider.onChange.Add(new EventDelegate(OnGenericOrganInfoEffortSliderChange));
    }

    public void SetOrganNameLabel(string organName)
    {
        NameLabel.text = organName;
    }

    public void SetHealthLabel(float percentage)
    {
        HealthLabel.text = "Health: " + (Mathf.Round(percentage * 100)) + " %";
    }

    public void OnGenericOrganInfoEffortSliderChange()
    {
        if (EffortSliderChangedEvent != null) EffortSliderChangedEvent(EffortSlider.value);
    }

    public void SetEffortValueLabel(float effort)
    {
        EffortValueLabel.text = Mathf.Round(effort * 100) + " %";
    }

    public void SetEffortSlider(float newValue)
    {
        EffortSlider.value = newValue;
    }

    public void UpdateOrganAlertIcons(OrganAlertType organAlertType, OrganAlertSeverness severness)
    {
        Color color;

        switch (severness)
        {
            case OrganAlertSeverness.Ok:
                color = Color.green;
                break;
            case OrganAlertSeverness.Warning:
                color = Color.yellow;
                break;
            case OrganAlertSeverness.Damage:
                color = Color.red;
                break;
            default:
                throw new ArgumentOutOfRangeException("severness");
        }

        switch (organAlertType)
        {
            case OrganAlertType.DeHydration:
                DeHydration.SetBgColor(color);
                break;
            case OrganAlertType.Starvation:
                Starvation.SetBgColor(color);
                break;
            case OrganAlertType.SuffocationReasonOxygen:
                SuffocationO.SetBgColor(color);
                break;
            case OrganAlertType.SuffocationReasonCarbonDioxide:
                SuffocationC.SetBgColor(color);
                break;
            case OrganAlertType.OverExhaustion:
                OverExhaustion.SetBgColor(color);
                break;
            default:
                throw new ArgumentOutOfRangeException("organAlertType");
        }
    }
}