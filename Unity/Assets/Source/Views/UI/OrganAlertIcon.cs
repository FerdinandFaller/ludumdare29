﻿using UnityEngine;

public class OrganAlertIcon : MonoBehaviour
{
    public UI2DSprite IconSprite;
    public UI2DSprite IconBackgroundSprite;

    public void SetBgColor(Color color)
    {
        IconBackgroundSprite.color = color;
    }
}
