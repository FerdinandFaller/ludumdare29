﻿using strange.extensions.mediation.impl;

public class MainMenuView : View
{
    public UIButton StartButton;

    [Inject]
    public MainMenuStartButtonClickedSignal StartButtonClickedSignal { get; set; }

    protected override void Start()
    {
        base.Start();

        StartButton.onClick.Add(new EventDelegate(OnStartButtonClicked));
    }

    private void OnStartButtonClicked()
    {
        StartButtonClickedSignal.Dispatch();
    }
}