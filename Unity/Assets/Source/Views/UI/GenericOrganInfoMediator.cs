﻿using System;
using strange.extensions.mediation.impl;
using UnityEngine;

public class GenericOrganInfoMediator : Mediator
{
    [Inject]
    public GenericOrganInfoView View { get; set; }
    [Inject]
    public OrganEffortChangeSignal EffortChangeSignal { get; set; }
    [Inject]
    public OrganEffortChangedSignal EffortChangedSignal { get; set; }
    [Inject]
    public OrganExhaustionChangedSignal ExhaustionChangedSignal { get; set; }
    [Inject]
    public OrganHealthChangedSignal HealthChangedSignal { get; set; }
    [Inject]
    public RequestSettingOfGenericOrganInfoSignal RequestSettingOfGenericOrganInfoSignal { get; set; }
    [Inject]
    public SetGenericOrganInfoSignal SetGenericOrganInfoSignal { get; set; }
    [Inject]
    public OrganAlertSignal OrganAlertSignal { get; set; }

    private OrganType _currentOrganType;


    public override void OnRegister()
    {
        View.EffortSliderChangedEvent -= OnEffortSliderChanged;
        View.EffortSliderChangedEvent += OnEffortSliderChanged;

        SetGenericOrganInfoSignal.AddListener(OnSetGenericOrganInfo);

        HealthChangedSignal.AddListener(OnModelHealthChanged);
        EffortChangedSignal.AddListener(OnModelEffortChanged);
        ExhaustionChangedSignal.AddListener(OnModelExhaustionChanged);
        OrganAlertSignal.AddListener(OnOrganAlert);
    }

    public override void OnRemove()
    {
        View.EffortSliderChangedEvent -= OnEffortSliderChanged;

        SetGenericOrganInfoSignal.RemoveListener(OnSetGenericOrganInfo);

        HealthChangedSignal.RemoveListener(OnModelHealthChanged);
        EffortChangedSignal.RemoveListener(OnModelEffortChanged);
        ExhaustionChangedSignal.RemoveListener(OnModelExhaustionChanged);
        OrganAlertSignal.RemoveListener(OnOrganAlert);
    }

    private void OnModelExhaustionChanged(OrganType organType, float newValue)
    {
        if (organType != _currentOrganType) return;

        RequestSettingOfGenericOrganInfoSignal.Dispatch(_currentOrganType);
    }

    private void OnModelEffortChanged(OrganType organType, float newVal)
    {
        if (organType != _currentOrganType) return;

        RequestSettingOfGenericOrganInfoSignal.Dispatch(_currentOrganType);
    }

    private void OnModelHealthChanged(OrganType organType, float newVal)
    {
        if (organType != _currentOrganType) return;

        RequestSettingOfGenericOrganInfoSignal.Dispatch(_currentOrganType);
    }

    private void OnOrganAlert(OrganType organType, OrganAlertType organAlertType, OrganAlertSeverness severness)
    {
        if (organType != _currentOrganType) return;

        View.UpdateOrganAlertIcons(organAlertType, severness);
    }

    private void OnEffortSliderChanged(float newValue)
    {
        EffortChangeSignal.Dispatch(_currentOrganType, newValue);
    }

    private void OnSetGenericOrganInfo(IOrganModel organModel)
    {
        _currentOrganType = organModel.OrganType;

        View.SetHealthLabel(organModel.Health);
        View.SetOrganNameLabel(organModel.OrganName);

        View.SetEffortSlider(organModel.Effort);
        View.SetEffortValueLabel(organModel.Effort);

        OnOrganAlert(organModel.OrganType, OrganAlertType.DeHydration, organModel.DeHydrationAlertSeverness);
        OnOrganAlert(organModel.OrganType, OrganAlertType.OverExhaustion, organModel.OverExhaustionAlertSeverness);
        OnOrganAlert(organModel.OrganType, OrganAlertType.Starvation, organModel.StarvationAlertSeverness);
        OnOrganAlert(organModel.OrganType, OrganAlertType.SuffocationReasonCarbonDioxide, organModel.SuffocationDueToCarbonDioxideAlertSeverness);
        OnOrganAlert(organModel.OrganType, OrganAlertType.SuffocationReasonOxygen, organModel.SuffocationDueToOxygenAlertSeverness);
    }
}