using System;
using strange.extensions.mediation.impl;
using UnityEngine;

public class StomachView : View
{
    public event Action MouseDownEvent;
    public SpriteRenderer BackgroundRenderer;
    public SpriteRenderer FillingRenderer;
    public Sprite GoodSprite;
    public Sprite BadSprite;
    public Sprite UglySprite;
    public Sprite FillingLowSprite;
    public Sprite FillingMediumSprite;
    public Sprite FillingFullSprite;


    void OnMouseDown()
    {
        if (MouseDownEvent != null) MouseDownEvent();
    }

    public void SetStatusSprite(OrganStatus status)
    {
        switch (status)
        {
            case OrganStatus.Good:
                BackgroundRenderer.sprite = GoodSprite;
                break;
            case OrganStatus.Bad:
                BackgroundRenderer.sprite = BadSprite;
                break;
            case OrganStatus.Ugly:
                BackgroundRenderer.sprite = UglySprite;
                break;
            default:
                throw new ArgumentOutOfRangeException("status");
        }
    }

    public void SetContentsVolumeSprite(float volume)
    {
        if (volume >= 0.9f)
        {
            FillingRenderer.sprite = FillingFullSprite;
        }
        else if (volume >= 0.49f)
        {
            FillingRenderer.sprite = FillingMediumSprite;
        }
        else if (volume >= 0.1f)
        {
            FillingRenderer.sprite = FillingLowSprite;
        }
        else
        {
            FillingRenderer.sprite = null;
        }
    }
}