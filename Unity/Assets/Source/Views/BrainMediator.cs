using System.Collections.Generic;
using strange.extensions.mediation.impl;

public class BrainMediator : Mediator
{
    [Inject]
    public BrainView View { get; set; }
    [Inject]
    public HideAllOrganGUIInfoViewsSignal HideAllOrganGuiInfoViewsSignal { get; set; }
    [Inject]
    public ShowHideGuiViewSignal ShowHideGuiViewSignal { get; set; }
    [Inject]
    public RequestSettingOfGenericOrganInfoSignal RequestSettingOfGenericOrganInfoSignal { get; set; }
    [Inject]
    public HeartBeatSignal HeartBeatSignal { get; set; }
    [Inject]
    public OrganStatusChangedSignal OrganStatusChangedSignal { get; set; }


    public override void OnRegister()
    {
        View.MouseDownEvent -= OnViewMouseDown;
        View.MouseDownEvent += OnViewMouseDown;

        HeartBeatSignal.AddListener(OnHeartBeat);
        OrganStatusChangedSignal.AddListener(OnOrganStatusChanged);
    }

    public override void OnRemove()
    {
        View.MouseDownEvent -= OnViewMouseDown;

        HeartBeatSignal.RemoveListener(OnHeartBeat);
        OrganStatusChangedSignal.RemoveListener(OnOrganStatusChanged);
    }

    private void OnOrganStatusChanged(OrganType organType, OrganStatus status)
    {
        if (organType == OrganType.Brain) View.SetStatusSprite(status);
    }

    private void OnHeartBeat(OrganType organType, Queue<Blood> bloods)
    {
        if (organType != OrganType.Brain) return;
    }

    private void OnViewMouseDown()
    {
        HideAllOrganGuiInfoViewsSignal.Dispatch();
        RequestSettingOfGenericOrganInfoSignal.Dispatch(OrganType.Brain);
        ShowHideGuiViewSignal.Dispatch(typeof(GenericOrganInfoView), true);
        ShowHideGuiViewSignal.Dispatch(typeof(BrainView), true);
    }
}