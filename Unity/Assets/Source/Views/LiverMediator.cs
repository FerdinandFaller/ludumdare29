using System.Collections.Generic;
using strange.extensions.mediation.impl;

public class LiverMediator : Mediator
{
    [Inject]
    public LiverView View { get; set; }

    [Inject]
    public HideAllOrganGUIInfoViewsSignal HideAllOrganGuiInfoViewsSignal { get; set; }
    [Inject]
    public ShowHideGuiViewSignal ShowHideGuiViewSignal { get; set; }
    [Inject]
    public RequestSettingOfGenericOrganInfoSignal RequestSettingOfGenericOrganInfoSignal { get; set; }
    [Inject]
    public RequestSettingOfLiverInfoSignal RequestSettingOfLiverInfoSignal { get; set; }
    [Inject]
    public SetLiverInfoSignal SetLiverInfoSignal { get; set; }
    [Inject]
    public HeartBeatSignal HeartBeatSignal { get; set; }
    [Inject]
    public OrganStatusChangedSignal OrganStatusChangedSignal { get; set; }


    public override void OnRegister()
    {
        View.MouseDownEvent -= OnViewMouseDown;
        View.MouseDownEvent += OnViewMouseDown;

        HeartBeatSignal.AddListener(OnHeartBeat);
        SetLiverInfoSignal.AddListener(OnSetLiverInfo);
        OrganStatusChangedSignal.AddListener(OnOrganStatusChanged);
    }

    public override void OnRemove()
    {
        View.MouseDownEvent -= OnViewMouseDown;

        HeartBeatSignal.RemoveListener(OnHeartBeat);
        SetLiverInfoSignal.RemoveListener(OnSetLiverInfo);
        OrganStatusChangedSignal.RemoveListener(OnOrganStatusChanged);
    }

    private void OnOrganStatusChanged(OrganType organType, OrganStatus status)
    {
        if (organType == OrganType.Liver) View.SetStatusSprite(status);
    }

    private void OnHeartBeat(OrganType organType, Queue<Blood> bloods)
    {
        if (organType != OrganType.Liver) return;
    }

    private void OnViewMouseDown()
    {
        HideAllOrganGuiInfoViewsSignal.Dispatch();
        RequestSettingOfGenericOrganInfoSignal.Dispatch(OrganType.Liver);
        RequestSettingOfLiverInfoSignal.Dispatch();
        ShowHideGuiViewSignal.Dispatch(typeof(GenericOrganInfoView), true);
        ShowHideGuiViewSignal.Dispatch(typeof(LiverView), true);
    }

    private void OnSetLiverInfo()
    {

    }
}