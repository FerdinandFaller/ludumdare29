using System.Collections.Generic;
using strange.extensions.mediation.impl;

public class LungMediator : Mediator
{
    [Inject]
    public LungView View { get; set; }

    [Inject]
    public HideAllOrganGUIInfoViewsSignal HideAllOrganGuiInfoViewsSignal { get; set; }
    [Inject]
    public ShowHideGuiViewSignal ShowHideGuiViewSignal { get; set; }
    [Inject]
    public RequestSettingOfGenericOrganInfoSignal RequestSettingOfGenericOrganInfoSignal { get; set; }
    [Inject]
    public RequestSettingOfLungInfoSignal RequestSettingOfLungInfoSignal { get; set; }
    [Inject]
    public SetLungInfoSignal SetLungInfoSignal { get; set; }
    [Inject]
    public OrganEffortChangedSignal EffortChangedSignal { get; set; }
    [Inject]
    public HeartBeatSignal HeartBeatSignal { get; set; }
    [Inject]
    public OrganStatusChangedSignal OrganStatusChangedSignal { get; set; }


    public override void OnRegister()
    {
        View.MouseDownEvent -= OnViewMouseDown;
        View.MouseDownEvent += OnViewMouseDown;

        HeartBeatSignal.AddListener(OnHeartBeat);
        SetLungInfoSignal.AddListener(OnSetLungInfo);
        OrganStatusChangedSignal.AddListener(OnOrganStatusChanged);
    }

    public override void OnRemove()
    {
        View.MouseDownEvent -= OnViewMouseDown;

        HeartBeatSignal.RemoveListener(OnHeartBeat);
        SetLungInfoSignal.RemoveListener(OnSetLungInfo);
        OrganStatusChangedSignal.RemoveListener(OnOrganStatusChanged);
    }

    private void OnOrganStatusChanged(OrganType organType, OrganStatus status)
    {
        if (organType == OrganType.Lung) View.SetStatusSprite(status);
    }

    private void OnHeartBeat(OrganType organType, Queue<Blood> bloods)
    {
        if (organType != OrganType.Lung) return;
    }

    private void OnViewMouseDown()
    {
        HideAllOrganGuiInfoViewsSignal.Dispatch();
        RequestSettingOfGenericOrganInfoSignal.Dispatch(OrganType.Lung);
        RequestSettingOfLungInfoSignal.Dispatch();
        ShowHideGuiViewSignal.Dispatch(typeof(GenericOrganInfoView), true);
        ShowHideGuiViewSignal.Dispatch(typeof(LungView), true);
    }

    private void OnSetLungInfo()
    {
        
    }
}