using System;
using strange.extensions.mediation.impl;
using UnityEngine;

public class BrainView : View
{
    public event Action MouseDownEvent;

    public SpriteRenderer Renderer;
    public Sprite GoodSprite;
    public Sprite BadSprite;
    public Sprite UglySprite;


    void OnMouseDown()
    {
        if (MouseDownEvent != null) MouseDownEvent();
    }

    public void SetStatusSprite(OrganStatus status)
    {
        switch (status)
        {
            case OrganStatus.Good:
                Renderer.sprite = GoodSprite;
                break;
            case OrganStatus.Bad:
                Renderer.sprite = BadSprite;
                break;
            case OrganStatus.Ugly:
                Renderer.sprite = UglySprite;
                break;
            default:
                throw new ArgumentOutOfRangeException("status");
        }
    }
}