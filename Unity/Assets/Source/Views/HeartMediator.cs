using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class HeartMediator : Mediator
{
    [Inject]
    public HeartView View { get; set; }

    [Inject]
    public HideAllOrganGUIInfoViewsSignal HideAllOrganGuiInfoViewsSignal { get; set; }
    [Inject]
    public ShowHideGuiViewSignal ShowHideGuiViewSignal { get; set; }
    [Inject]
    public RequestSettingOfGenericOrganInfoSignal RequestSettingOfGenericOrganInfoSignal { get; set; }
    [Inject]
    public RequestSettingOfHeartInfoSignal RequestSettingOfHeartInfoSignal { get; set; }
    [Inject]
    public SetHeartInfoSignal SetHeartInfoSignal { get; set; }
    [Inject]
    public OrganEffortChangedSignal OrganEffortChangedSignal { get; set; }
    [Inject]
    public OrganExhaustionChangedSignal OrganExhaustionChangedSignal { get; set; }
    [Inject]
    public HeartBeatSignal HeartBeatSignal { get; set; }
    [Inject]
    public OrganStatusChangedSignal OrganStatusChangedSignal { get; set; }


    public override void OnRegister()
    {
        View.MouseDownEvent -= OnViewMouseDown;
        View.MouseDownEvent += OnViewMouseDown;

        OrganEffortChangedSignal.AddListener(OnModelEffortChanged);
        OrganExhaustionChangedSignal.AddListener(OnModelExhaustionChanged);
        HeartBeatSignal.AddListener(OnHeartBeat);
        SetHeartInfoSignal.AddListener(OnSetHeartInfo);
        OrganStatusChangedSignal.AddListener(OnModelOrganStatusChanged);
    }

    public override void OnRemove()
    {
        View.MouseDownEvent -= OnViewMouseDown;

        OrganEffortChangedSignal.RemoveListener(OnModelEffortChanged);
        OrganExhaustionChangedSignal.RemoveListener(OnModelExhaustionChanged);
        HeartBeatSignal.RemoveListener(OnHeartBeat);
        SetHeartInfoSignal.RemoveListener(OnSetHeartInfo);
        OrganStatusChangedSignal.RemoveListener(OnModelOrganStatusChanged);
    }

    private void OnModelOrganStatusChanged(OrganType organType, OrganStatus status)
    {
        if(organType == OrganType.Heart)View.SetStatusSprite(status);
    }

    private void OnHeartBeat(OrganType organType, Queue<Blood> bloods)
    {
        if (organType != OrganType.Heart) return;

        View.PlayPumpAnimation();
    }

    private void OnModelEffortChanged(OrganType organType, float newVal)
    {
        if (organType != OrganType.Heart) return;
        RequestSettingOfHeartInfoSignal.Dispatch();
    }

    private void OnModelExhaustionChanged(OrganType organType, float newVal)
    {
        if (organType != OrganType.Heart) return;
        RequestSettingOfHeartInfoSignal.Dispatch();
    }

    private void OnViewMouseDown()
    {
        HideAllOrganGuiInfoViewsSignal.Dispatch();
        RequestSettingOfGenericOrganInfoSignal.Dispatch(OrganType.Heart);
        RequestSettingOfHeartInfoSignal.Dispatch();
        ShowHideGuiViewSignal.Dispatch(typeof(GenericOrganInfoView), true);
        ShowHideGuiViewSignal.Dispatch(typeof(HeartView), true);
    }

    private void OnSetHeartInfo(uint bloodPerTick, float heartBeatFrequency)
    {
        View.SetBloodPerTickLabel(bloodPerTick);
        View.SetHeartBeatFrequencyLabel(heartBeatFrequency);
        View.SetPumpAnimationFramerate(Mathf.RoundToInt(heartBeatFrequency * 60f));
    }
}