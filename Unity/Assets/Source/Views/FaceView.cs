﻿using System;
using strange.extensions.mediation.impl;
using UnityEngine;

public class FaceView : View
{
    public SpriteRenderer Renderer;
    public Sprite DeadSprite;
    public Sprite SickSprite;
    public Sprite NeutralSprite;
    public Sprite HappySprite;


    public void SetFaceStatusSprite(FaceStatus status)
    {
        switch (status)
        {
            case FaceStatus.Dead:
                Renderer.sprite = DeadSprite;
                break;
            case FaceStatus.Sick:
                Renderer.sprite = SickSprite;
                break;
            case FaceStatus.Neutral:
                Renderer.sprite = NeutralSprite;
                break;
            case FaceStatus.Happy:
                Renderer.sprite = HappySprite;
                break;
            default:
                throw new ArgumentOutOfRangeException("status");
        }
    }
}