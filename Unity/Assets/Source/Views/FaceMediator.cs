using strange.extensions.mediation.impl;

public class FaceMediator : Mediator
{
    [Inject]
    public FaceView View { get; set; }
    [Inject]
    public FaceStatusSignal FaceStatusSignal { get; set; }


    public override void OnRegister()
    {
        FaceStatusSignal.AddListener(OnFaceStatus);
    }

    public override void OnRemove()
    {
        FaceStatusSignal.RemoveListener(OnFaceStatus);
    }

    private void OnFaceStatus(FaceStatus status)
    {
        View.SetFaceStatusSprite(status);
    }
}