using strange.extensions.signal.impl;

public class OrganEffortChangedSignal : Signal<OrganType, float>
{
}