using strange.extensions.signal.impl;

/// <summary>
/// Params:
/// OrganType - Of the Organ that dispatched the signal
/// float - The Amount by which the Health should be changed
/// </summary>
public class OrganHealthChangeSignal : Signal<OrganType, float>
{
    
}