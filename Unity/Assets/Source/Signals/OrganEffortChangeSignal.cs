using strange.extensions.signal.impl;

public class OrganEffortChangeSignal : Signal<OrganType, float>
{
}