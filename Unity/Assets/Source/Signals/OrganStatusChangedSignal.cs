using strange.extensions.signal.impl;

public class OrganStatusChangedSignal : Signal<OrganType, OrganStatus>
{
    
}