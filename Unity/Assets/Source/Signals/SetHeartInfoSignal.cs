using strange.extensions.signal.impl;

/// <summary>
/// Params: BloodPerTick, TickFrequency
/// </summary>
public class SetHeartInfoSignal : Signal<uint, float>
{
    
}