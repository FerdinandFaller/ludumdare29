using strange.extensions.signal.impl;

/// <summary>
/// IFood: The Food to eat
/// bool: Force it in if too big?
/// </summary>
public class EatSignal : Signal<IFood, bool>
{
    
}