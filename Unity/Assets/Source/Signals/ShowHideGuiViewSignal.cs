﻿using System;
using strange.extensions.signal.impl;

public class ShowHideGuiViewSignal : Signal<Type, bool>
{
}