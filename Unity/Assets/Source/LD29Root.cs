﻿using System;
using System.Collections.Generic;
using strange.extensions.context.impl;

public class LD29Root : ContextView
{
    public Dictionary<Type, UIPanel> GuiViews = new Dictionary<Type, UIPanel>();

    public UIPanel GenericOrganInfoPanel;
    public UIPanel MainMenuPanel;
    public UIPanel GameUIPanel;
    public UIPanel HeartInfoPanel;
    public UIPanel LungInfoPanel;
    public UIPanel LiverInfoPanel;
    public UIPanel StomachInfoPanel;
    public UIPanel BrainInfoPanel;

    public const float SimulationSpeedFactor = 1440f / 5f; // 1440min(24h) : 5min (288)


    void Awake()
    {
        context = new LD29Context(this, true);

        GuiViews.Add(typeof(MainMenuView), MainMenuPanel);
        GuiViews.Add(typeof(GameUIView), GameUIPanel);
        GuiViews.Add(typeof(HeartView), HeartInfoPanel);
        GuiViews.Add(typeof(LungView), LungInfoPanel);
        GuiViews.Add(typeof(LiverView), LiverInfoPanel);
        GuiViews.Add(typeof(StomachView), StomachInfoPanel);
        GuiViews.Add(typeof(BrainView), BrainInfoPanel);
        GuiViews.Add(typeof(GenericOrganInfoView), GenericOrganInfoPanel);

        foreach (var panels in GuiViews.Values)
        {
            panels.alpha = 0;
        }

        context.Start();
    }
}