﻿using strange.extensions.command.impl;

public class DigestCommand : Command
{
    [Inject]
    public StomachModel StomachModel { get; set; }


    public override void Execute()
    {
        if (StomachModel.Contents.Count <= 0) return;

        var food = StomachModel.Contents.Peek();

        float digestedNutrients;
        float digestedHarmfulSubstances;
        float digestedWater;
        food.DigestTick(StomachModel.Effort, out digestedNutrients, out digestedHarmfulSubstances, out digestedWater);
        StomachModel.HarmfulSubstancesExcess += digestedHarmfulSubstances;
        StomachModel.NutrientsExcess += digestedNutrients;
        StomachModel.WaterExcess += digestedWater;
        StomachModel.RemoveNextFood(false);
    }
}