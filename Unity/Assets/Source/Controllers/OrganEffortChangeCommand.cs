﻿using strange.extensions.command.impl;

public class OrganEffortChangeCommand : Command
{
    [Inject]
    public OrganType OrganType { get; set; }

    /// <summary>
    /// [0..1]
    /// </summary>
    [Inject]
    public float NewValue { get; set; }


    public override void Execute()
    {
        var organModel = this.GetOrganModelFromOrganType(OrganType);
        organModel.Effort = NewValue;
    }
}