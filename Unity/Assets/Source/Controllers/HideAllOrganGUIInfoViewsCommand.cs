﻿using strange.extensions.command.impl;

public class HideAllOrganGUIInfoViewsCommand : Command
{
    [Inject]
    public ShowHideGuiViewSignal ShowHideGuiViewSignal { get; set; }

    public override void Execute()
    {
        ShowHideGuiViewSignal.Dispatch(typeof(GenericOrganInfoView), false);
        ShowHideGuiViewSignal.Dispatch(typeof(HeartView), false);
        ShowHideGuiViewSignal.Dispatch(typeof(BrainView), false);
        ShowHideGuiViewSignal.Dispatch(typeof(LiverView), false);
        ShowHideGuiViewSignal.Dispatch(typeof(LungView), false);
        ShowHideGuiViewSignal.Dispatch(typeof(StomachView), false);
    }
}