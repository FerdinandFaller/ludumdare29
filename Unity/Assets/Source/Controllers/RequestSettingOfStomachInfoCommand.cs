﻿using strange.extensions.command.impl;

public class RequestSettingOfStomachInfoCommand : Command
{
    [Inject]
    public SetStomachInfoSignal SetStomachInfoSignal { get; set; }


    public override void Execute()
    {
        SetStomachInfoSignal.Dispatch();
    }
}