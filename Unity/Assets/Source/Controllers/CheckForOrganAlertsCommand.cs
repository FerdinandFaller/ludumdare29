﻿using strange.extensions.command.impl;

public class CheckForOrganAlertsCommand : Command
{
    [Inject]
    public OrganType OrganType { get; set; }
    [Inject]
    public OrganAlertSignal OrganAlertSignal { get; set; }


    public override void Execute()
    {
        var organModel = this.GetOrganModelFromOrganType(OrganType);

        // Starvation
        OrganAlertSignal.Dispatch(organModel.OrganType, OrganAlertType.Starvation, organModel.StarvationAlertSeverness);

        // DeHydration
        OrganAlertSignal.Dispatch(organModel.OrganType, OrganAlertType.DeHydration, organModel.DeHydrationAlertSeverness);

        // Suffocation Reason Oxygen
        OrganAlertSignal.Dispatch(organModel.OrganType, OrganAlertType.SuffocationReasonOxygen, organModel.SuffocationDueToOxygenAlertSeverness);

        // Suffocation Reason CarbonDioxide
        OrganAlertSignal.Dispatch(organModel.OrganType, OrganAlertType.SuffocationReasonCarbonDioxide, organModel.SuffocationDueToCarbonDioxideAlertSeverness);

        // OverExhaustion
        OrganAlertSignal.Dispatch(organModel.OrganType, OrganAlertType.OverExhaustion, organModel.OverExhaustionAlertSeverness);
    }
}