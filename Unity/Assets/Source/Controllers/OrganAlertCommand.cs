﻿using System;
using strange.extensions.command.impl;

public class OrganAlertCommand : Command
{
    [Inject]
    public OrganType OrganType { get; set; }
    [Inject]
    public OrganAlertType OrganAlertType { get; set; }
    [Inject]
    public OrganAlertSeverness OrganAlertSeverness { get; set; }
    [Inject]
    public OrganHealthDecreaseSignal OrganHealthDecreaseSignal { get; set; }


    public override void Execute()
    {
        switch (OrganAlertSeverness)
        {
            case OrganAlertSeverness.Ok:
                break;
            case OrganAlertSeverness.Warning:
                break;
            case OrganAlertSeverness.Damage:
                OrganHealthDecreaseSignal.Dispatch(OrganType, OrganAlertType);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}