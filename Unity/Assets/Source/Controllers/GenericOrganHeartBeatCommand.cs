﻿using System.Collections.Generic;
using strange.extensions.command.impl;

public class GenericOrganHeartBeatCommand : Command
{
    [Inject]
    public IOrganModel GenericOrganModel { get; set; }
    [Inject]
    public Queue<Blood> Bloods { get; set; }


    public override void Execute()
    {
        foreach (var blood in Bloods)
        {
            // Nutrients
            if (GenericOrganModel.NutrientDeficit > 0)
            {
                // More Demand than Supply
                if (GenericOrganModel.NutrientDeficit > blood.Nutrients)
                {
                    GenericOrganModel.NutrientDeficit -= blood.Nutrients;
                    blood.Nutrients = 0;
                }
                else
                {
                    blood.Nutrients -= GenericOrganModel.NutrientDeficit;
                    GenericOrganModel.NutrientDeficit = 0;
                }
            }

            // Oxygen
            if (GenericOrganModel.OxygenDeficit > 0)
            {
                // More Demand than Supply
                if (GenericOrganModel.OxygenDeficit > blood.Oxygen)
                {
                    GenericOrganModel.OxygenDeficit -= blood.Oxygen;
                    blood.Oxygen = 0;
                }
                else
                {
                    blood.Oxygen -= GenericOrganModel.OxygenDeficit;
                    GenericOrganModel.OxygenDeficit = 0;
                }
            }

            // Water
            if (GenericOrganModel.WaterDeficit > 0)
            {
                // More Demand than Supply
                if (GenericOrganModel.WaterDeficit > blood.Water)
                {
                    GenericOrganModel.WaterDeficit -= blood.Water;
                    blood.Water = 0;
                }
                else
                {
                    blood.Water -= GenericOrganModel.WaterDeficit;
                    GenericOrganModel.WaterDeficit = 0;
                }
            }

            // CarbonDioxide
            if (GenericOrganModel.CarbonDioxideExcess > 0)
            {
                // More Supply than Demand
                if (GenericOrganModel.CarbonDioxideExcess > Blood.CarbonDioxideMax - blood.CarbonDioxide)
                {
                    GenericOrganModel.CarbonDioxideExcess -= Blood.CarbonDioxideMax - blood.CarbonDioxide;
                    blood.CarbonDioxide = Blood.CarbonDioxideMax;
                }
                else
                {
                    blood.CarbonDioxide += GenericOrganModel.CarbonDioxideExcess;
                    GenericOrganModel.CarbonDioxideExcess = 0;
                }
            }
        }
    }
}