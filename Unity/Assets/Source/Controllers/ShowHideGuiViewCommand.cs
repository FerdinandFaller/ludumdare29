using System;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using UnityEngine;

public class ShowHideGuiViewCommand : Command
{
    [Inject(ContextKeys.CONTEXT_VIEW)]
    public GameObject ContextView { get; set; }

    [Inject]
    public Type GuiViewType { get; set; }

    [Inject]
    public bool ShouldBeVisible { get; set; }


    public override void Execute()
    {
        var root = ContextView.GetComponent<LD29Root>();
        UIPanel panel = root.GuiViews[GuiViewType];

        if (panel.alpha > 0 && !ShouldBeVisible)
        {
            panel.alpha = 0;
        }
        else if (Math.Abs(panel.alpha) < 0.0001f && ShouldBeVisible)
        {
            panel.alpha = 1;
        }
    }
}