﻿using strange.extensions.command.impl;

public class StartCommand : Command
{
    public override void Execute()
    {
        var showHideGuiSignal = injectionBinder.GetInstance<ShowHideGuiViewSignal>();
        showHideGuiSignal.Dispatch(typeof(MainMenuView), true);
    }
}