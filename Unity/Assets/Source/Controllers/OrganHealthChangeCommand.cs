using strange.extensions.command.impl;

public class OrganHealthChangeCommand : Command
{
    [Inject]
    public OrganType OrganType { get; set; }

    /// <summary>
    /// [0..1]
    /// </summary>
    [Inject]
    public float Amount { get; set; }


    public override void Execute()
    {
        var organModel = this.GetOrganModelFromOrganType(OrganType);
        organModel.Health += Amount;
    }
}