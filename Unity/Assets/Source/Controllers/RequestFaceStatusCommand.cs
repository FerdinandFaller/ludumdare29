using System.Collections.Generic;
using System.Linq;
using strange.extensions.command.impl;

/// <summary>
/// Determines the Face Status.
/// Happy: All Organs full Health and no OrganAlertSeverness.Warning or worse
/// Neutral: All Organs Full Health
/// Sick: At least one Organ has not full Health
/// Dead: At least one Organ with 0 Health
/// </summary>
public class RequestFaceStatusCommand : Command
{
    [Inject]
    public FaceStatusSignal FaceStatusSignal { get; set; }

    [Inject]
    public BrainModel BrainModel { get; set; }
    [Inject]
    public HeartModel HeartModel { get; set; }
    [Inject]
    public LiverModel LiverModel { get; set; }
    [Inject]
    public LungModel LungModel { get; set; }
    [Inject]
    public StomachModel StomachModel { get; set; }

    private List<IOrganModel> _organs;


    public override void Execute()
    {
        _organs = new List<IOrganModel> {BrainModel, HeartModel, LiverModel, LungModel, StomachModel};

        // Happy
        var isHappy = true;
        foreach (var organ in _organs)
        {
            if (organ.Health < 1)
            {
                isHappy = false;
                break;
            }

            var severnesses = new List<OrganAlertSeverness>
            {
                organ.DeHydrationAlertSeverness,
                organ.OverExhaustionAlertSeverness,
                organ.StarvationAlertSeverness,
                organ.SuffocationDueToCarbonDioxideAlertSeverness,
                organ.SuffocationDueToOxygenAlertSeverness
            };

            isHappy = severnesses.All(organAlertSeverness => organAlertSeverness != OrganAlertSeverness.Warning && organAlertSeverness != OrganAlertSeverness.Damage);
            if (!isHappy) break;
        }
        if (isHappy)
        {
            FaceStatusSignal.Dispatch(FaceStatus.Happy);
            return;
        }

        // Neutral
        var isNeutral = _organs.All(organ => !(organ.Health < 1));
        if (isNeutral)
        {
            FaceStatusSignal.Dispatch(FaceStatus.Neutral);
            return;
        }

        // Sick
        var isSick = _organs.Any(organ => organ.Health < 1);
        if (isSick)
        {
            FaceStatusSignal.Dispatch(FaceStatus.Sick);
            return;
        }

        // Sick
        var isDead = _organs.Any(organ => organ.Health <= 0);
        if (isDead)
        {
            FaceStatusSignal.Dispatch(FaceStatus.Dead);
            return;
        }
    }
}