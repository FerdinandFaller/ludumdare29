using System;
using strange.extensions.command.impl;

public class OrganHealthDecreaseCommand : Command
{
    [Inject]
    public OrganType OrganType { get; set; }
    [Inject]
    public OrganAlertType OrganAlertType { get; set; }
    [Inject]
    public OrganHealthChangeSignal OrganHealthChangeSignal { get; set; }


    public override void Execute()
    {
        var organModel = this.GetOrganModelFromOrganType(OrganType);
        float amount;

        switch (OrganAlertType)
        {
            case OrganAlertType.DeHydration:
                amount = -organModel.DamageWhenDeHydrating;
                break;
            case OrganAlertType.Starvation:
                amount = -organModel.DamageWhenStarving;
                break;
            case OrganAlertType.SuffocationReasonOxygen:
                amount = -organModel.DamageWhenSuffocatingDueToOxygen;
                break;
            case OrganAlertType.SuffocationReasonCarbonDioxide:
                amount = -organModel.DamageWhenSuffocatingDueToCarbonDioxide;
                break;
            case OrganAlertType.OverExhaustion:
                amount = -organModel.DamageWhenOverExhausting;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        OrganHealthChangeSignal.Dispatch(OrganType, amount);
    }
}