﻿using strange.extensions.command.impl;

public class DetermineOrganStatusCommand : Command
{
    [Inject]
    public OrganType OrganType { get; set; }


    public override void Execute()
    {
        var organModel = this.GetOrganModelFromOrganType(OrganType);

        // At the moment we only determine by Health and distinguisgh between Good, Bad and Ugly Status. The OrganType currently is ignored.
        if (organModel.Health > 0.66f) organModel.OrganStatus = OrganStatus.Good;
        else if (organModel.Health > 0.33f) organModel.OrganStatus = OrganStatus.Bad;
        else organModel.OrganStatus = OrganStatus.Ugly;
    }
}