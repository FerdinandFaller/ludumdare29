using strange.extensions.command.impl;
using strange.extensions.context.api;
using UnityEngine;

public class MainMenuStartButtonClickedCommand : Command
{
    [Inject(ContextKeys.CONTEXT_VIEW)]
    public GameObject ContextView { get; set; }
    [Inject]
    public ShowHideGuiViewSignal ShowHideGuiViewSignal { get; set; }
    
    [Inject]
    public UpdateSignal UpdateSignal { get; set; }

    [Inject]
    public EatSignal EatSignal { get; set; }


    public override void Execute()
    {
        var gameLoop = ContextView.AddComponent<GameLoop>();
        //injectionBinder.Bind<GameLoop>().ToValue(gameLoop);
        gameLoop.UpdateSignal = UpdateSignal;

        Application.LoadLevel("Game");

        ShowHideGuiViewSignal.Dispatch(typeof(MainMenuView), false);
        ShowHideGuiViewSignal.Dispatch(typeof(GameUIView), true);

        gameLoop.StartLoop();

        EatSignal.Dispatch(new GenericFoodRation(), true);
        EatSignal.Dispatch(new GenericFoodRation(), true);
        EatSignal.Dispatch(new GenericFoodRation(), true);
        EatSignal.Dispatch(new GenericFoodRation(), true);
        EatSignal.Dispatch(new GenericFoodRation(), true);
        EatSignal.Dispatch(new GenericFoodRation(), true);
        EatSignal.Dispatch(new GenericFoodRation(), true);
        EatSignal.Dispatch(new GenericFoodRation(), true);
        EatSignal.Dispatch(new MineralWater(), true);
    }
}