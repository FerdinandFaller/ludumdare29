﻿using strange.extensions.command.impl;

public class RequestAverageOrganHealthCommand : Command
{
    [Inject]
    public AverageOrganHealthSignal AverageOrganHealthSignal { get; set; }

    [Inject]
    public BrainModel BrainModel { get; set; }
    [Inject]
    public HeartModel HeartModel { get; set; }
    [Inject]
    public LiverModel LiverModel { get; set; }
    [Inject]
    public LungModel LungModel { get; set; }
    [Inject]
    public StomachModel StomachModel { get; set; }


    public override void Execute()
    {
        float averageHealth = 0f;

        averageHealth += BrainModel.Health;
        averageHealth += HeartModel.Health;
        averageHealth += LiverModel.Health;
        averageHealth += LungModel.Health;
        averageHealth += StomachModel.Health;

        averageHealth /= 5;

        AverageOrganHealthSignal.Dispatch(averageHealth);
    }
}