﻿using System;
using strange.extensions.command.impl;

public class RequestSettingOfGenericOrganInfoCommand : Command
{
    [Inject]
    public OrganType OrganType { get; set; }
    [Inject]
    public SetGenericOrganInfoSignal SetGenericOrganInfoSignal { get; set; }


    public override void Execute()
    {
        var organModel = this.GetOrganModelFromOrganType(OrganType);
        SetGenericOrganInfoSignal.Dispatch(organModel);
    }
}