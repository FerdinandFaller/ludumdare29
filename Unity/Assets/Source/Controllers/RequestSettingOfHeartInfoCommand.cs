﻿using strange.extensions.command.impl;
using UnityEngine;

public class RequestSettingOfHeartInfoCommand : Command
{
    [Inject]
    public HeartModel HeartModel { get; set; }
    [Inject]
    public SetHeartInfoSignal SetHeartInfoSignal { get; set; }


    public override void Execute()
    {
        SetHeartInfoSignal.Dispatch(HeartModel.BloodAmountPerBeat, HeartModel.HeartBeatFrequency);
    }
}