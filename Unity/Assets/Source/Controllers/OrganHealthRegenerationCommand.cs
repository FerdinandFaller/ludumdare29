﻿using strange.extensions.command.impl;

public class OrganHealthRegenerationCommand : Command
{
    [Inject]
    public OrganType OrganType { get; set; }
    [Inject]
    public OrganHealthChangeSignal OrganHealthChangeSignal { get; set; }


    public override void Execute()
    {
        var organModel = this.GetOrganModelFromOrganType(OrganType);

        OrganHealthChangeSignal.Dispatch(OrganType, organModel.HealthRegenerationPerTick);
    }
}