﻿using strange.extensions.command.impl;
using UnityEngine;

public class SimulationTickCommand : Command
{
    [Inject]
    public UpdateOrganExhaustionSignal UpdateOrganExhaustionSignal { get; set; }
    [Inject]
    public DigestSignal DigestSignal { get; set; }
    [Inject]
    public DetermineOrganStatusSignal DetermineOrganStatusSignal { get; set; }
    [Inject]
    public IncreaseOrganDeficitsAndExcessesSignal IncreaseOrganDeficitsAndExcessesSignal { get; set; }
    [Inject]
    public CheckForOrganAlertsSignal CheckForOrganAlertsSignal { get; set; }
    [Inject]
    public OrganHealthRegenerationSignal OrganHealthRegenerationSignal { get; set; }
    [Inject]
    public RequestFaceStatusSignal RequestFaceStatusSignal { get; set; }
    [Inject]
    public BloodCircuit BloodCircuit { get; set; }
    [Inject]
    public float SmoothDeltaTime { get; set; }


    public override void Execute()
    {
        UpdateOrgan(OrganType.Body);
        UpdateOrgan(OrganType.Lung);
        UpdateOrgan(OrganType.Heart);
        UpdateOrgan(OrganType.Brain);
        UpdateOrgan(OrganType.Stomach);
        UpdateOrgan(OrganType.Liver);

        RequestFaceStatusSignal.Dispatch();
        
        Debug.Log("Blood Averages:");
        Debug.Log("Nutrients: " + BloodCircuit.GetAverageAmountOfNutrients() + " / " + Blood.NutrientsMax);
        Debug.Log("CarbonDioxide: " + BloodCircuit.GetAverageAmountOfCarbonDioxide() + " / " + Blood.CarbonDioxideMax);
        Debug.Log("HarmfulSubstances: " + BloodCircuit.GetAverageAmountOfHarmfulSubstances() + " / " + Blood.HarmfulSubstancesMax);
        Debug.Log("Leucocytes: " + BloodCircuit.GetAverageAmountOfLeucocytes() + " / " + Blood.LeucocytesMax);
        Debug.Log("Oxygen: " + BloodCircuit.GetAverageAmountOfOxygen() + " / " + Blood.OxygenMax);
        Debug.Log("Water: " + BloodCircuit.GetAverageAmountOfWater() + " / " + Blood.WaterMax);
    }

    private void UpdateOrgan(OrganType organType)
    {
        OrganHealthRegenerationSignal.Dispatch(organType);
        UpdateOrganExhaustionSignal.Dispatch(organType);
        IncreaseOrganDeficitsAndExcessesSignal.Dispatch(organType);
        if (organType == OrganType.Stomach) DigestSignal.Dispatch();
        DetermineOrganStatusSignal.Dispatch(organType);
        CheckForOrganAlertsSignal.Dispatch(organType);
    }
}