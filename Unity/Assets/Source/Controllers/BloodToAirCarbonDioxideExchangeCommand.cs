using System.Collections.Generic;
using System.Linq;
using strange.extensions.command.impl;

public class BloodToAirCarbonDioxideExchangeCommand : Command
{
    [Inject]
    public Queue<Blood> Bloods { get; set; }
    [Inject]
    public LungModel Lung { get; set; }

    public override void Execute()
    {
        // How much CarbonDioxide is currently in the Blood?
        var carbonDioxideInBlood = Bloods.Sum(blood => blood.CarbonDioxide);

        var carbonDioxideToDrain = carbonDioxideInBlood * Lung.CarbonDioxideExchangeFactor;
        var carbonDioxideToAdd = carbonDioxideToDrain;

        // Take that amount out of the blood
        foreach (var blood in Bloods)
        {
            if (carbonDioxideToDrain >= blood.CarbonDioxide)
            {
                carbonDioxideToDrain -= blood.CarbonDioxide;
                blood.CarbonDioxide = 0;
            }
            else
            {
                blood.CarbonDioxide -= carbonDioxideToDrain;
                carbonDioxideToDrain = 0;
                break;
            }
        }

        carbonDioxideToAdd -= carbonDioxideToDrain; // We reduce the amount by what was left in carbonDioxideToDrain to get the real amount we drained
        // And put it into the air in the Lung
        foreach (var air in Lung.AirQueue)
        {
            var maxCarbonDioxideIntake = Air.CarbonDioxideMax - air.CarbonDioxide;

            if (maxCarbonDioxideIntake < carbonDioxideToAdd)
            {
                air.CarbonDioxide = Air.CarbonDioxideMax;
                carbonDioxideToAdd -= maxCarbonDioxideIntake;
            }
            else
            {
                air.CarbonDioxide += carbonDioxideToAdd;
                carbonDioxideToAdd = 0;
                break;
            }
        }
    }
}