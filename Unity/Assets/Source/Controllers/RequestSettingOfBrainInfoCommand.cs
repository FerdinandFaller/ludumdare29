using strange.extensions.command.impl;

public class RequestSettingOfBrainInfoCommand : Command
{
    [Inject]
    public SetBrainInfoSignal SetBrainInfoSignal { get; set; }


    public override void Execute()
    {
        SetBrainInfoSignal.Dispatch();
    }
}