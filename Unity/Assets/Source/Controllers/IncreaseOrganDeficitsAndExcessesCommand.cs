﻿using System;
using strange.extensions.command.impl;
using UnityEngine;

public class IncreaseOrganDeficitsAndExcessesCommand : Command
{
    [Inject]
    public OrganType OrganType { get; set; }


    public override void Execute()
    {
        var organModel = this.GetOrganModelFromOrganType(OrganType);

        organModel.NutrientDeficit += organModel.NutrientConsumption;
        organModel.OxygenDeficit += organModel.OxygenConsumption;
        organModel.WaterDeficit += organModel.WaterConsumption;
        organModel.CarbonDioxideExcess += organModel.CarbonDioxideProduction;

        switch (OrganType)
        {
            case OrganType.Body:
                ((BodyModel)organModel).LeucocytesExcess += ((BodyModel)organModel).LeucocytesProduction;
                break;
            case OrganType.Heart:
                
                break;
            case OrganType.Brain:
                
                break;
            case OrganType.Liver:
                var liverModel = (LiverModel)organModel;
                liverModel.HarmfulSubstancesDeficit += liverModel.HarmfulSubstancesConsumption;
                break;
            case OrganType.Lung:
                // Gas Exchange handled in HeartBeatCommand
                break;
            case OrganType.Stomach:
                // Adding handled in DigestCommand
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        // TODO: Make sure that Organs that produce AND consume the same thing get valid Deficit and Excess Vars
    }
}