﻿using strange.extensions.command.impl;

public class RequestSettingOfLiverInfoCommand : Command
{
    [Inject]
    public SetLiverInfoSignal SetLiverInfoSignal { get; set; }


    public override void Execute()
    {
        SetLiverInfoSignal.Dispatch();
    }
}