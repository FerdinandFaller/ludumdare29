﻿using strange.extensions.command.impl;

public class EatCommand : Command
{
    [Inject]
    public IFood Food { get; set; }
    [Inject]
    public bool ForceToEat { get; set; }
    [Inject]
    public StomachModel StomachModel { get; set; }


    public override void Execute()
    {
        StomachModel.AddFood(Food, ForceToEat);
    }
}