using strange.extensions.command.impl;

public class BreathOutCommand : Command
{
    [Inject]
    public Surrounding Surrounding { get; set; }
    [Inject]
    public LungModel Lung { get; set; }

    public override void Execute()
    {
        if (Lung.AirQueue.Count <= 0) return;

        Surrounding.ReturnAir(Lung.AirQueue);
    }
}