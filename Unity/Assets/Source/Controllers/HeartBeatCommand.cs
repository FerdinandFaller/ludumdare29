﻿using System;
using System.Collections.Generic;
using strange.extensions.command.impl;
using UnityEngine;

public class HeartBeatCommand : Command
{
    [Inject]
    public OrganType OrganType { get; set; }
    [Inject]
    public Queue<Blood> Bloods { get; set; } 
    [Inject]
    public GenericOrganHeartBeatSignal GenericOrganHeartBeatSignal { get; set; }
    [Inject]
    public AirToBloodOxygenExchangeSignal AirToBloodOxygenExchangeSignal { get; set; }
    [Inject]
    public BloodToAirCarbonDioxideExchangeSignal BloodToAirCarbonDioxideExchangeSignal { get; set; }


    public override void Execute()
    {
        switch (OrganType)
        {
            case OrganType.Body:
                var bodyModel = injectionBinder.GetInstance<BodyModel>();
                GenericOrganHeartBeatSignal.Dispatch(bodyModel, Bloods);

                foreach (var blood in Bloods)
                {
                    if (bodyModel.LeucocytesExcess > 0)
                    {
                        // More Supply than Demand
                        if (bodyModel.LeucocytesExcess > Blood.LeucocytesMax - blood.Leucocytes)
                        {
                            bodyModel.LeucocytesExcess -= Blood.LeucocytesMax - blood.Leucocytes;
                            blood.Leucocytes = Blood.LeucocytesMax;
                        }
                        else
                        {
                            blood.Leucocytes += bodyModel.LeucocytesExcess;
                            bodyModel.LeucocytesExcess = 0;
                        }
                    }
                }
                break;
            case OrganType.Heart:
                var heartModel = injectionBinder.GetInstance<HeartModel>();
                GenericOrganHeartBeatSignal.Dispatch(heartModel, Bloods);
                AudioController.Play("HeartBeat");
                break;
            case OrganType.Brain:
                var brainModel = injectionBinder.GetInstance<BrainModel>();
                GenericOrganHeartBeatSignal.Dispatch(brainModel, Bloods);
                break;
            case OrganType.Liver:
                var liverModel = injectionBinder.GetInstance<LiverModel>();
                GenericOrganHeartBeatSignal.Dispatch(liverModel, Bloods);

                foreach (var blood in Bloods)
                {
                    if (liverModel.HarmfulSubstancesDeficit > 0)
                    {
                        // More Demand than Supply
                        if (liverModel.HarmfulSubstancesDeficit > blood.HarmfulSubstances)
                        {
                            liverModel.HarmfulSubstancesDeficit -= blood.HarmfulSubstances;
                            blood.HarmfulSubstances = 0;
                        }
                        else
                        {
                            blood.HarmfulSubstances -= liverModel.HarmfulSubstancesDeficit;
                            liverModel.HarmfulSubstancesDeficit = 0;
                        }
                    }
                }
                break;
            case OrganType.Lung:
                var lungModel = injectionBinder.GetInstance<LungModel>();
                GenericOrganHeartBeatSignal.Dispatch(lungModel, Bloods);
                AirToBloodOxygenExchangeSignal.Dispatch(Bloods);
                BloodToAirCarbonDioxideExchangeSignal.Dispatch(Bloods);
                break;
            case OrganType.Stomach:
                var stomachModel = injectionBinder.GetInstance<StomachModel>();
                GenericOrganHeartBeatSignal.Dispatch(stomachModel, Bloods);

                // Add To Blood
                foreach (var blood in Bloods)
                {
                    // Nutrients
                    if (stomachModel.NutrientsExcess > 0)
                    {
                        // More Supply than space
                        if (stomachModel.NutrientsExcess > Blood.NutrientsMax - blood.Nutrients)
                        {
                            stomachModel.NutrientsExcess -= Blood.NutrientsMax - blood.Nutrients;
                            blood.Nutrients = Blood.NutrientsMax;
                        }
                        else
                        {
                            blood.Nutrients += stomachModel.NutrientsExcess;
                            stomachModel.NutrientsExcess = 0;
                        }
                    }

                    // Harmful Substances
                    if (stomachModel.HarmfulSubstancesExcess > 0)
                    {
                        // More Supply than space
                        if (stomachModel.HarmfulSubstancesExcess > Blood.HarmfulSubstancesMax - blood.HarmfulSubstances)
                        {
                            stomachModel.HarmfulSubstancesExcess -= Blood.HarmfulSubstancesMax - blood.HarmfulSubstances;
                            blood.HarmfulSubstances = Blood.HarmfulSubstancesMax;
                        }
                        else
                        {
                            blood.HarmfulSubstances += stomachModel.HarmfulSubstancesExcess;
                            stomachModel.HarmfulSubstancesExcess = 0;
                        }
                    }

                    // Water
                    if (stomachModel.WaterExcess > 0)
                    {
                        // More Supply than space
                        if (stomachModel.WaterExcess > Blood.WaterMax - blood.Water)
                        {
                            stomachModel.WaterExcess -= Blood.WaterMax - blood.Water;
                            blood.Water = Blood.WaterMax;
                        }
                        else
                        {
                            blood.Water += stomachModel.WaterExcess;
                            stomachModel.WaterExcess = 0;
                        }
                    }
                }
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}