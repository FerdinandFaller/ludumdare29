using strange.extensions.command.impl;
using UnityEngine;

public class BreathInCommand : Command
{
    [Inject]
    public Surrounding Surrounding { get; set; }
    [Inject]
    public LungModel Lung { get; set; }

    public override void Execute()
    {
        Debug.Log("BreathIn");
        if (Lung.AirQueue.Count > 0) throw new BreathInBeforeBreathOutException();

        var airs = Surrounding.GetAir(Lung.LungVolume);

        foreach (var air in airs)
        {
            Lung.AirQueue.Enqueue(air);
        }
    }
}