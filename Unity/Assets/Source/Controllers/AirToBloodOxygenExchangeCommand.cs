using System.Collections.Generic;
using System.Linq;
using strange.extensions.command.impl;

public class AirToBloodOxygenExchangeCommand : Command
{
    [Inject]
    public Queue<Blood> Bloods { get; set; }
    [Inject]
    public LungModel Lung { get; set; }
 

    public override void Execute()
    {
        // How much oxygen can fit into the blood?
        var diffToMaxOxygen = Bloods.Sum(blood => Blood.OxygenMax - blood.Oxygen);

        var oxygenToDrain = diffToMaxOxygen * Lung.OxygenExchangeFactor;
        var oxygenToAdd = diffToMaxOxygen * Lung.OxygenExchangeFactor;

        // Take that amount out of the air in the lung
        foreach (var air in Lung.AirQueue)
        {
            if (oxygenToDrain >= air.Oxygen)
            {
                oxygenToDrain -= air.Oxygen;
                air.Oxygen = 0;
            }
            else
            {
                air.Oxygen -= oxygenToDrain;
                oxygenToDrain = 0;
                break;
            }
        }

        oxygenToAdd -= oxygenToDrain; // We reduce the amount by what was left in oxygenToDrain to get the real amount we drained
        // And put it into the blood
        foreach (var blood in Bloods)
        {
            var maxOxygenIntake = Blood.OxygenMax - blood.Oxygen;

            if (maxOxygenIntake < oxygenToAdd)
            {
                blood.Oxygen = Blood.OxygenMax;
                oxygenToAdd -= maxOxygenIntake;
            }
            else
            {
                blood.Oxygen += oxygenToAdd;
                oxygenToAdd = 0;
                break;
            }
        }
    }
}