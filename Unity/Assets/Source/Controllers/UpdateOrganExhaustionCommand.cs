﻿using strange.extensions.command.impl;

public class UpdateOrganExhaustionCommand : Command
{
    [Inject]
    public OrganType OrganType { get; set; }


    public override void Execute()
    {
        var organModel = this.GetOrganModelFromOrganType(OrganType);
        organModel.Exhaustion += (organModel.Effort - 0.5f) * organModel.ExhaustionFactor;
    }
}