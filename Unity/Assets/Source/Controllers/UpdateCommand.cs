using System.Collections.Generic;
using strange.extensions.command.impl;
using UnityEngine;

public class UpdateCommand : Command
{
    [Inject]
    public SimulationTickSignal SimulationTickSignal { get; set; }
    [Inject]
    public HeartBeatSignal HeartBeatSignal { get; set; }
    [Inject]
    public BreathInSignal BreathInSignal { get; set; }
    [Inject]
    public BreathOutSignal BreathOutSignal { get; set; }
    [Inject]
    public BloodCircuit BloodCircuit { get; set; }
    [Inject]
    public HeartModel Heart { get; set; }
    [Inject]
    public LungModel Lung { get; set; }
    [Inject]
    public float SmoothDeltaTime { get; set; }

    private static float _simulationTickTimer;
    private static float _heartBeatTimer;
    private static float _breathingTimer;


    public override void Execute()
    {
        _heartBeatTimer += SmoothDeltaTime;
        _breathingTimer += SmoothDeltaTime;

        if (_breathingTimer > 1 / Lung.BreathingFrequency)
        {
            BreathOutSignal.Dispatch();
            BreathInSignal.Dispatch();
            _breathingTimer = 0;
        }

        if (_heartBeatTimer > 1 / Heart.HeartBeatFrequency)
        {
            HeartBeat();
            _heartBeatTimer = 0;
        }

        if (SimulationTickSignal != null)
        {
            _simulationTickTimer += SmoothDeltaTime;

            // 1 Update per Second
            if (_simulationTickTimer > 1)
            {
                SimulationTickSignal.Dispatch(_simulationTickTimer);
                _simulationTickTimer = 0;
            }
        }
    }

    private void HeartBeat()
    {
        var bloodAmountThisBeat = Heart.BloodAmountPerBeat;

        // Body
        var bloods = GetBlood(bloodAmountThisBeat);
        HeartBeatSignal.Dispatch(OrganType.Body, bloods);
        ReturnBlood(bloods);

        // Lung
        bloods = GetBlood(bloodAmountThisBeat);
        HeartBeatSignal.Dispatch(OrganType.Lung, bloods);
        ReturnBlood(bloods);

        // Heart
        bloods = GetBlood(bloodAmountThisBeat);
        HeartBeatSignal.Dispatch(OrganType.Heart, bloods);
        ReturnBlood(bloods);

        ReturnBlood(GetBlood(BloodCircuit.HeartToBrainDistance));

        // Brain
        bloods = GetBlood(bloodAmountThisBeat);
        HeartBeatSignal.Dispatch(OrganType.Brain, bloods);
        ReturnBlood(bloods);

        ReturnBlood(GetBlood(BloodCircuit.BrainToStomachDistance));

        // Stomach
        bloods = GetBlood(bloodAmountThisBeat);
        HeartBeatSignal.Dispatch(OrganType.Stomach, bloods);
        ReturnBlood(bloods);

        ReturnBlood(GetBlood(BloodCircuit.StomachToLiverDistance));

        // Liver
        bloods = GetBlood(bloodAmountThisBeat);
        HeartBeatSignal.Dispatch(OrganType.Liver, bloods);
        ReturnBlood(bloods);

        ReturnBlood(GetBlood(BloodCircuit.LiverToHeartDistance));
    }

    private Queue<Blood> GetBlood(uint amount)
    {
        var bloods = new Queue<Blood>();
        for (int i = 0; i < amount; i++)
        {
            bloods.Enqueue(BloodCircuit.BloodQueue.Dequeue());
        }
        return bloods;
    }

    private void ReturnBlood(IEnumerable<Blood> bloods)
    {
        foreach (var blood in bloods)
        {
            BloodCircuit.BloodQueue.Enqueue(blood);
        }
    }
}