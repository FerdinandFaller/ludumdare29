﻿using strange.extensions.command.impl;

public class RequestSettingOfLungInfoCommand : Command
{
    [Inject]
    public SetLungInfoSignal SetLungInfoSignal { get; set; }


    public override void Execute()
    {
        SetLungInfoSignal.Dispatch();
    }
}